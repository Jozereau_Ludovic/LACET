$(function () {
	$('#calendar-holder').fullCalendar({
		defaultView: 'agendaWeek',
        lazyFetching: true,
		timezone: ('Europe/Paris'),
		firstDay: 1,
        slotLabelFormat: 'HH:mm',
		businessHours: {
		    start: '08:00',
		    end: '20:00',
		    dow: [1, 2, 3, 4, 5, 6]
		},
		minTime: '07:00',
		maxTime: '20:00',
		slotDuration: '00:15:00',
		allDaySlot: false,
		allDayDefault: false,
		weekends: true,
		editable: true,
		slotEventOverlap: false,
		height: "auto",
    	
        eventSources: [
            {
                url: '/full-calendar/load',
                type: 'POST',
                data: {},
                error: function () {}
            },
			{
				url: $('#edtFile').val(),
			}
        ],
        
        eventRender: function eventRender(event, element, view) {
			if (event.default == false) {
				element.css('border-color', 'black');
			}
	
			if ($('#role').val().indexOf('ROLE_RESPONSABLE') != -1) {	
				return $('#licence_selector').val() == 'all'
					|| event.className == $('#licence_selector').val();
			}
			else if ($('#role').val().indexOf('ROLE_PROF') != -1) {
				return event.prof == $('#nom_prenom').val();				
			}
        },
        
        eventMouseover: function (data, event, view) {			
			tooltip = '<div class="tooltiptopicevent"'
				+ 'style="width:auto; height:auto; background:#17c4ad; '
				+ 'border:{width:10px; color:black}; position:absolute; z-index:10001; '
				+ 'padding:10px 10px 10px 10px;  line-height: 200%;">' 
				+ 'Cours : ' + data.title + '</br>'
				+ 'Licence : ' + data.className + '</br>'
				+ 'Enseignant : ' + data.prof + '</br>'
				+ 'Lieu : ' + data.lieu + '</br>'
				+ '</div>';

			$("body").append(tooltip);
			
			$(this).mouseover(function (e) {
				$(this).css('z-index', 10000);
				$('.tooltiptopicevent').fadeIn('500');
				$('.tooltiptopicevent').fadeTo('10', 1.9);
			}).mousemove(function (e) {
				$('.tooltiptopicevent').css('top', e.pageY + 10);
				$('.tooltiptopicevent').css('left', e.pageX + 20);
			});


		},
		eventMouseout: function (data, event, view) {
			$(this).css('z-index', 8);

			$('.tooltiptopicevent').remove();
		},
		
		eventDrop: function(event, delta, revertFunc) {
			if ($('#role').val().indexOf('ROLE_ADMIN') != -1) {
				sauvegarderChangements();
			}
			else {
				revertFunc();
			}
		},
		
		eventClick: function(calEvent, jsEvent, view) {
			if ($('#role').val().indexOf('ROLE_ADMIN') != -1) {
				var lieu = prompt('Lieu :', calEvent.lieu, {
					buttons: {
						Valider : true,
						Annuler : false
					}
				});
				
				if (lieu) {
					var prof = prompt('Enseignant :', calEvent.prof, {
						buttons: {
							Valider : true,
							Annuler : false
						}
					});
					
					if (prof) {
						calEvent.prof = prof;					
					}
					
					calEvent.lieu = lieu;
					$('#calendar-holder').fullCalendar('updateEvent', calEvent);
					sauvegarderChangements();
				}
			}
		},
    });
    
    $('#licence_selector').change(function() {
		$('#calendar-holder').fullCalendar('rerenderEvents');
	});
	
	function sauvegarderChangements() {
		var events = $('#calendar-holder').fullCalendar('clientEvents');
		
		$.ajax({			
			url: "/generation/sauvegarderChangements",
			data: "events=" + JSON.stringify(events)
				+ "&edt=" + $('#edtFile').val(),
			type: 'POST',
		});
	}
		
});
