$( function() {
	$('.choice_licences').change(function() {
		var licences = document.getElementsByName("staps_generateurbundle_parametres[licences][]");
		
		for (i = 0; i < licences.length; i++) {
			var effectif = $('.effectifL' + (i + 1).toString());
			
			effectif.attr('disabled', !licences[i].checked);
			effectif.attr('required', licences[i].checked);
		}
	});
});
