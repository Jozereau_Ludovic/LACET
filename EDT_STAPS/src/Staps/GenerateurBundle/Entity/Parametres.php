<?php

namespace Staps\GenerateurBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parametres
 */
class Parametres {
	public function __construct() {
		$this->datetime = date("Y-m-d H:i:s");
		$this->nom = $this->datetime;
	}
	/**
	 * @var string
	 */
	private $nom; 
	
	/**
	 * @var boolean
	 */
	private $toutPlacer; 
	
    /**
     * @var \DateTime
     */
    private $dureeCreneau;

    /**
     * @var \DateTime
     */
    private $debutPeriode;

    /**
     * @var \DateTime
     */
    private $finPeriode;
    
    /**
     * @var int
     */
    private $semestre; 
    
    /**
     * @var string
     */
    private $licences; 

    /**
     * @var int
     */
    private $capaciteCM;

    /**
     * @var int
     */
    private $capaciteTP;

    /**
     * @var int
     */
    private $capaciteTD;

    /**
     * @var int
     */
    private $effectifPrevuL1;
    
    /**
     * @var int
     */
    private $effectifPrevuL2;
    
    /**
     * @var int
     */
    private $effectifPrevuL3;
    
    /**
     * @var string
     */ 
	private $datetime;
	
	/**
     * Set nom
     *
     * @param string $nom
     *
     * @return Parametres
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

	/**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set toutPlacer
     *
     * @param boolean $toutPlacer
     *
     * @return Parametres
     */
    public function setToutPlacer($toutPlacer)
    {
        $this->toutPlacer = $toutPlacer;

        return $this;
    }

    /**
     * Get toutPlacer
     *
     * @return boolean
     */
    public function getToutPlacer()
    {
        return $this->toutPlacer;
    }
    
    /**
     * Set dureeCreneau
     *
     * @param \DateTime $dureeCreneau
     *
     * @return Parametres
     */
    public function setDureeCreneau($dureeCreneau)
    {
        $this->dureeCreneau = $dureeCreneau;

        return $this;
    }

    /**
     * Get dureeCreneau
     *
     * @return \DateTime
     */
    public function getDureeCreneau()
    {
        return $this->dureeCreneau;
    }

    /**
     * Set debutPeriode
     *
     * @param \DateTime $debutPeriode
     *
     * @return Parametres
     */
    public function setDebutPeriode($debutPeriode)
    {
        $this->debutPeriode = $debutPeriode;

        return $this;
    }

    /**
     * Get debutPeriode
     *
     * @return \DateTime
     */
    public function getDebutPeriode()
    {
        return $this->debutPeriode;
    }

    /**
     * Set finPeriode
     *
     * @param \DateTime $finPeriode
     *
     * @return Parametres
     */
    public function setFinPeriode($finPeriode)
    {
        $this->finPeriode = $finPeriode;

        return $this;
    }

    /**
     * Get finPeriode
     *
     * @return \DateTime
     */
    public function getFinPeriode()
    {
        return $this->finPeriode;
    }

    /**
     * Set semestre
     *
     * @param integer $semestre
     *
     * @return Parametres
     */
    public function setSemestre($semestre)
    {
        $this->semestre = $semestre;

        return $this;
    }

    /**
     * Get semestre
     *
     * @return integer
     */
    public function getSemestre()
    {
        return $this->semestre;
    }
    
    /**
     * Set licences
     *
     * @param integer $licences
     *
     * @return Parametres
     */
    public function setLicences($licences)
    {
        $this->licences = $licences;

        return $this;
    }

    /**
     * Get licences
     *
     * @return string
     */
    public function getLicences()
    {
        return $this->licences;
    }

    /**
     * Set capaciteCM
     *
     * @param integer $capaciteCM
     *
     * @return Parametres
     */
    public function setCapaciteCM($capaciteCM)
    {
        $this->capaciteCM = $capaciteCM;

        return $this;
    }

    /**
     * Get capaciteCM
     *
     * @return integer
     */
    public function getCapaciteCM()
    {
        return $this->capaciteCM;
    }

    /**
     * Set capaciteTP
     *
     * @param integer $capaciteTP
     *
     * @return Parametres
     */
    public function setCapaciteTP($capaciteTP)
    {
        $this->capaciteTP = $capaciteTP;

        return $this;
    }

    /**
     * Get capaciteTP
     *
     * @return integer
     */
    public function getCapaciteTP()
    {
        return $this->capaciteTP;
    }

    /**
     * Set capaciteTD
     *
     * @param integer $capaciteTD
     *
     * @return Parametres
     */
    public function setCapaciteTD($capaciteTD)
    {
        $this->capaciteTD = $capaciteTD;

        return $this;
    }

    /**
     * Get capaciteTD
     *
     * @return integer
     */
    public function getCapaciteTD()
    {
        return $this->capaciteTD;
    }

    /**
     * Set effectifPrevuL1
     *
     * @param integer $effectifPrevuL1
     *
     * @return Parametres
     */
    public function setEffectifPrevuL1($effectifPrevuL1)
    {
        $this->effectifPrevuL1 = $effectifPrevuL1;

        return $this;
    }

    /**
     * Get effectifPrevuL1
     *
     * @return integer
     */
    public function getEffectifPrevuL1()
    {
        return $this->effectifPrevuL1;
    }
    
    /**
     * Set effectifPrevuL2
     *
     * @param integer $effectifPrevuL2
     *
     * @return Parametres
     */
    public function setEffectifPrevuL2($effectifPrevuL2)
    {
        $this->effectifPrevuL2 = $effectifPrevuL2;

        return $this;
    }

    /**
     * Get effectifPrevuL2
     *
     * @return integer
     */
    public function getEffectifPrevuL2()
    {
        return $this->effectifPrevuL2;
    }
    
    /**
     * Set effectifPrevuL3
     *
     * @param integer $effectifPrevuL3
     *
     * @return Parametres
     */
    public function setEffectifPrevuL3($effectifPrevuL3)
    {
        $this->effectifPrevuL3 = $effectifPrevuL3;

        return $this;
    }

    /**
     * Get effectifPrevuL3
     *
     * @return integer
     */
    public function getEffectifPrevuL3()
    {
        return $this->effectifPrevuL3;
    }
    
    /**
     * Get datetime
     *
     * @return string
     */
    public function getDatetime()
    {
        return $this->datetime;
    }
    
    public function getCapacite($typeCours) {
		switch ($typeCours) {
			case 'CM': return $this->capaciteCM;
			case 'TD': return $this->capaciteTD;
			case 'TP': return $this->capaciteTP;
		}
	}
	
    public function getEffectifPrevu($licence) {
		switch ($licence) {
			case '1': return $this->effectifPrevuL1;
			case '2': return $this->effectifPrevuL2;
			case '3': return $this->effectifPrevuL3;
		}
	}
}
