<?php

namespace Staps\GenerateurBundle\Form;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ParametresType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
			->add('nom', TextType::class,
				array(
					'label' => 'Nom de l\'emploi du temps',
				)
			)
			->add('toutPlacer', ChoiceType::class,
				array(
					'choices' => array(
						'Placer tous les cours' => true,
						'Ne placer que les cours "sûrs"' => false,
					),
					'expanded' => true,
					'multiple' => false,
					'label' => 'Placement des cours',
				)
			)
			->add('dureeCreneau', TimeType::class,
		    	array(
					'html5' => false,
		    		'widget' => 'single_text',
		    		'label' => 'Durée d\'un créneau',
		    	)
		    )
			->add('debutPeriode', DateType::class,
		    	array(
					'html5' => false,
		    		'widget' => 'single_text',
		    		'attr' => array('class' => 'js-datepicker'), 
		    		'label' => 'Date de début',
		    	)
		    )
			->add('finPeriode', DateType::class,
		    	array(
					'html5' => false,
		    		'widget' => 'single_text',
		    		'attr' => array('class' => 'js-datepicker'), 
		    		'label' => 'Date de fin',
		    	)
		    )
		    ->add('semestre', ChoiceType::class,
        		array(
        			'choices' => array(
        				'Semestre 1' => 1,
        				'Semestre 2' => 2,
        			),
         			'expanded' => true,
		        	'multiple' => false,
		        	'label' => 'Semestre',
		        )
		    )
		    ->add('licences', ChoiceType::class,
        		array(
        			'choices' => array(
        				'L1' => 1,
        				'L2' => 2,
        				'L3' => 3,
        			),
         			'expanded' => true,
		        	'multiple' => true,
		        	'label' => 'Licence(s)',
		        	'data' => array(1, 2, 3),
		        )
		    )
			->add('capaciteCM', IntegerType::class,
		    	array(
					'attr' => array('min' => 100),
		    		'label' => 'Capacité d\'un CM',
		    		'data' => 200,
		    	)
		    )
		    ->add('capaciteTD', IntegerType::class,
		    	array(
					'attr' => array('min' => 20),
		    		'label' => 'Capacité d\'un TD',
		    		'data' => 40,
		    	)
		    )
			->add('capaciteTP', IntegerType::class,
		    	array(
					'attr' => array('min' => 10),
		    		'label' => 'Capacité d\'un TP',
		    		'data' => 20,
		    	)
		    )
			->add('effectifPrevuL1', IntegerType::class,
		    	array(
					'attr' => array('min' => 100),
		    		'label' => 'Effectif prévu pour la L1',
		    		'data' => 400,
		    	)
		    )
			->add('effectifPrevuL2', IntegerType::class,
		    	array(
					'attr' => array('min' => 100),
		    		'label' => 'Effectif prévu pour la L2',
		    		'data' => 200,
		    	)
		    )
			->add('effectifPrevuL3', IntegerType::class,
		    	array(
					'attr' => array('min' => 100),
		    		'label' => 'Effectif prévu pour la L3',
		    		'data' => 200,
		    	)
		    )
			->add('submit', SubmitType::class,
		    	array(
		    		'label' => 'Lancer la génération',
		    	)
		    )
		;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Staps\GenerateurBundle\Entity\Parametres'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'staps_generateurbundle_parametres';
    }


}
