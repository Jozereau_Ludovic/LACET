<?php

namespace Staps\GenerateurBundle\Repository;

/**
 * ParametresRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ParametresRepository extends \Doctrine\ORM\EntityRepository {
	public function findByDate() {
		$date = $this->createQueryBuilder('d')
			->select('MAX(d.date) AS max_date')
			->getQuery()->getResult()
		;
		
		return $this->createQueryBuilder('p')
			->where('p.date = :date')
			->setParameter('date', $date)
			->getQuery()->getResult()
		;
	}
	
}
