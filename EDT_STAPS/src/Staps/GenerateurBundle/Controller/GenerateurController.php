<?php

namespace Staps\GenerateurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Staps\GenerateurBundle\Entity\Parametres;
use Staps\GenerateurBundle\Form\ParametresType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class GenerateurController extends Controller {
    /**
     * Affiche la page d'entrée des paramètres de la génération d'emploi du temps.
     * 
     * @param {
     *  $request Request
     * }
     * 
     * @return la vue de la page de traitement ou la vue de la page d'entrée des paramètres
     */
     
    /**
	 * @Security("has_role('ROLE_PROF')")
	 */
    public function entreeAction(Request $request) {
		$em = $this->getDoctrine()->getManager();
		$maquette = $em->getRepository('StapsCoursBundle:EC')->findAll();
		
		$prof = $this->container->get('security.token_storage')->getToken()->getUser()->getEnseignant();
		
		if (empty($maquette)) {
			return $this->render('StapsGenerateurBundle:Generateur:entreeVide.html.twig');
		}
		
		// Création du formulaire des paramètres
		$parametres = new Parametres();
		$form = $this->createForm(ParametresType::class, $parametres);
		
		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {	
			$edt = $this->generationEmploiTemps($parametres);
			
			$this->sauvegarderEdt($edt, $parametres);
			
			$datetime = $parametres->getDatetime();
			
			file_put_contents("agenda/parametres_$datetime.json", $this->parametresToJson($parametres));
			file_put_contents("debug_param", json_last_error_msg());
			
			return $this->render('StapsGenerateurBundle:Generateur:traitement.html.twig', 
				array('parametres' => json_decode(file_get_contents("agenda/parametres_$datetime.json"), true),
					'edt' => "/agenda/edt_$datetime.json", 'nom_prenom' => $prof->getNom()." ".$prof->getPrenom()));
		}
		
		$files = scandir("agenda");
		$datetimes_noms = array();
		foreach ($files as $file) {
			if (strpos($file, 'parametres_') !== false) {			
				$params = json_decode(file_get_contents("agenda/$file"), true);
				
				$datetimes_noms[] = 
					array('datetime' => substr($file, strlen("parametres_"), strlen("YYYY-MM-dd HH:mm:ii")),
					'nom' => $params['nom']);
			}
		}
		
        return $this->render('StapsGenerateurBundle:Generateur:entree.html.twig', 
			array('form' => $form->createView(), 'datetimes_noms' => $datetimes_noms, ));
    }
    
    /**
     * Affiche la page de traitement d'une emploi du temps généré précédemment.
     * 
     * @return la vue de la page de traitement
     */
     
    /**
	 * @Security("has_role('ROLE_PROF')")
	 */
    public function afficherEdtAction() {
		$datetime = $_GET['datetime'];
		
		$prof = $this->container->get('security.token_storage')->getToken()->getUser()->getEnseignant();
			
		return $this->render('StapsGenerateurBundle:Generateur:traitement.html.twig',
			array('parametres' => json_decode(file_get_contents("agenda/parametres_$datetime.json"), true),
				'edt' => "/agenda/edt_$datetime.json", 'nom_prenom' => $prof->getNom()." ".$prof->getPrenom()));
	}
    
    /**
     * Sauvegarde l'emploi du temps généré dans un fichier json.
     * 
     * @param {
     *  $edt structure de données de l'emploi du temps
     *  $parametres Staps\GenerateurBundle\Entity\Parametres
     * }
     */
    private function sauvegarderEdt($edt, $parametres) {
		$em = $this->getDoctrine()->getManager();
		
		$rows = array();
		$debutPeriode = clone $parametres->getDebutPeriode();
		$numSemaine = -1;

		foreach ($edt as $semaine) {
			$numSemaine++;
			$numJour = -1;
			
			foreach ($semaine as $jour) {
				$numJour++;
				$numCreneau = -1;
				
				foreach ($jour as $creneau) {
					$numCreneau++;
							
					foreach ($creneau as $tuple) {										
						$ec = $em->getRepository('StapsCoursBundle:EC')->find($tuple['ec']);
							
						$title = "Séance n°".$tuple['groupe']." ";
						$id = $ec->getNumero();	
							
						if ($tuple['apsa'] != null) {
							$apsa = $em->getRepository('StapsCoursBundle:APSA')->find($tuple['apsa']);
								
							$id = $id.intval($apsa->getId());
							$title = $title.$apsa->getIntitule();
						}
						else {
							$title = $title.$ec->getIntitule();
						}
						
						switch ($ec->getTypeCours()) {
							case 'CM' : $id = $id."0";
							break;
							
							case 'CM' : $id = $id."1";
							break;
							
							case 'CM' : $id = $id."2";
							break;
						}						
						$id = $id.strval($tuple['groupe']);					
						
						if ($tuple['lieu'] == -1) {
							$lieu = "Salle de ".$ec->getTypeCours()."\n";
						}
						else {
							$lieu = $em->getRepository('StapsCoursBundle:Lieu')->find($tuple['lieu']);
								
							$lieu = $lieu->getLocalite()." ".$lieu->getInstallation()." ".$lieu->getZoneActivite();
						}

						($tuple['prof'] == -1) ? $prof = "Enseignant lambda\n"
							: $prof = "Nom enseignant\n";
						
						$intervalleJour = $numSemaine * 7 + $numJour;
						$intervalleTemps = 
							$numCreneau * getdate($parametres->getDureeCreneau()->getTimestamp())['hours'] + 8;
						$start = (clone $debutPeriode)
							->add(new \DateInterval("P".$intervalleJour."DT".$intervalleTemps."H"));
						$end = (clone $start)
							->add(new \DateInterval("PT".getdate($parametres->getDureeCreneau()->getTimestamp())['hours']."H"));
							
						switch (substr($ec->getNumero(), 0, 1)) {
							case 1:
							case 2: $className = 'L1';
							break;
							case 3:
							case 4: $className = 'L2';
							break;
							case 5:
							case 6: $className = 'L3';
							break;
						}
						
						$default = $tuple['default'];

						$rows[] = array('id' => $id, 'title' => $title, 'start' => $start->format('Y-m-d\TH:i'), 
						'end' => $end->format('Y-m-d\TH:i'), 'className' => $className, 
						'lieu' => $lieu, 'prof' => $prof, 'default' => $default);
					}
				}
			}
		}
		
		$datetime = $parametres->getDatetime();

		file_put_contents("agenda/edt_$datetime.json", json_encode($rows));
		file_put_contents("debug", json_last_error_msg());
	}
	
    /**
     * Calcule le nombre de créneaux à faire dans une journée à partir de $dureeCreneau
     * 
     * @param {
     *  $dureeCreneau datetime
     * }
     * 
     * @return integer
     */
	private function getNbCreneaux($dureeCreneau) {
		$dureeCreneau = $dureeCreneau->format('H') * 60 + $dureeCreneau->format('i');
		
		return floor(12 * 60/$dureeCreneau);
	}
	
    /**
     * Tri les EC par APSA, parcours, effectif et heures
     * 
     * @param {
     *  $a \Staps\CoursBundle\Entity\EC
     *  $b \Staps\CoursBundle\Entity\EC
     * }
     * 
     * @return integer
     */
	private function tri_prio_desc_EC(\Staps\CoursBundle\Entity\EC $a, \Staps\CoursBundle\Entity\EC $b) {
		$poids = 0;
		
		count($a->getAPSAs()) > count($b->getAPSAs()) ? $poids += -1 : $poids += 1;

		count(explode(',', $a->getParcours())) > count(explode(',', $b->getParcours()))
			? $poids += -1 : $poids += 1;
		
		$a->getEffectif() > $b->getEffectif() ? $poids += -1 : $poids += 1;
		
		$a->getHeures() > $b->getHeures() ? $poids += -1 : $poids += 1;
		
		return $poids;
	}
	
	/**
     * Génère l'emploi du temps.
     * 
     * @param {
     *  $parametres \Staps\GenerateurBundle\Entity\Parametres
     * }
     * 
     * @return structure de données de l'emploi du temps
     */
     
    /**
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function generationEmploiTemps($parametres) {
		$em = $this->getDoctrine()->getManager();
		
		// Tri et filtre des EC
		$ec = $em->getRepository('StapsCoursBundle:EC')
			->findBySemestreAndLicences($parametres->getSemestre(), $parametres->getLicences());

		// Récupération nombre semaines et nombre créneaux
		$nbSemaines = ceil(date_diff($parametres->getDebutPeriode(), $parametres->getFinPeriode(), true)->format('%a')/7);
		$nbCreneaux = $this->getNbCreneaux($parametres->getDureeCreneau());	
		
		// Intialisation des variables
		$this->initialiserCartes($ec, $nbCreneaux);

		$ecFixes = array();
		$debutPeriode = clone $parametres->getDebutPeriode();
		
		$edt = 
			array_fill(0, $nbSemaines,
				array_fill(0, 6,
					array_fill(0, $nbCreneaux, array()) // dans lesquels on met array(ec, groupe, apsa, lieu, prof)
				)
			)
		;
		
		// Boucle extérieure sur les semaines
		for ($semaine = 0; $semaine < $nbSemaines; ++$semaine) {
			$ecBug = array();
			$apsaBug = array();
			usort($ec, array($this, "tri_prio_desc_EC"));
			
			// Premier tour : on fixe les APSA
			foreach ($ec as $e) {
				if (count($e->getAPSAs()) != 0 && $e->estPlacable($semaine%2, 
						$em->getRepository('StapsCoursBundle:EC')->findByCode($e->getCoursPrecedent()))) {
					foreach ($e->getAPSAs() as $apsa) {
						for ($jour = 0; $jour < 7; ++$jour) {
							// On calcule la date de traitement
							$intervalleJour = $semaine * 7 + $jour;
							$dateActu = (clone $debutPeriode)->add(new \DateInterval("P".$intervalleJour."D"));
							
							// Si cette date est dépassée, on retourne l'emploi du temps actuel
							if ($dateActu > $parametres->getFinPeriode()) {
								return $edt;
							}
							
							// On ne traite pas les dimanches
							if ($dateActu->format('D') == 'Sun') {
								continue;
							}
							
							for ($creneau = 0; $creneau < $nbCreneaux; ++$creneau) {
								// On calcule l'heure de traitement
								$intervalleTemps = 
									$creneau * getdate($parametres->getDureeCreneau()->getTimestamp())['hours'] + 8;

								$heureActu = (clone $debutPeriode)
									->add(new \DateInterval("P".$intervalleJour."DT".$intervalleTemps."H"));
								
								
								// On ne traite pas les samedi après-midi
								if ($dateActu->format('D') == 'Sat' && getdate($heureActu->getTimestamp())['hours'] > 12) {
									break;
								}
								
								// On récupère les lieux pour cette APSA
								$lieux = $em->getRepository('StapsCoursBundle:Lieu')->findByAPSA($apsa);
								$eventsParents = $em->getRepository('StapsCalendrierBundle:EventParent')->findByLieu($lieux);
								
								$idLieux = $this->filtreMoment('lieu', $eventsParents, $heureActu);
								$this->filtreID('lieu', $idLieux, $edt[$semaine][$jour][$creneau]);
								
								if (!(empty($idLieux))) {
									// On récupère les enseignants pour cette APSA
									$profs = $em->getRepository('StapsUserBundle:Enseignant')->findByAPSA($apsa);
									$eventsParents = $em->getRepository('StapsCalendrierBundle:EventParent')->findByEnseignant($profs);

									$idProfs = $this->filtreMoment('prof', $eventsParents, $heureActu);
									$this->filtreID('prof', $idProfs, $edt[$semaine][$jour][$creneau]);
																		
									$e->setCartePlacesCreneau($jour, $creneau, $idLieux, $idProfs);
								}							
							}
						}
					
						$nbGroupes = $apsa->getNbGroupes($parametres, $e);
						
						$this->placerCreneaux($edt[$semaine], $parametres, $nbGroupes, $e, $apsa);
						$this->initialiserCartes($e, $nbCreneaux);
						
						// Si tous les groupes de l'APSA n'ont pas été placés
						if ($apsa->getGroupesPlaces() != $nbGroupes) {
							$apsaBug[] = $apsa; // On met l'APSA dans le liste des APSA qui posent problème
						}
					}
					// Si on a placé toutes les heures du cours
					if ($e->estFixe()) {
						$ecFixes[] = $e; // On le place dans la liste des EC fixés
						array_splice($ec, array_search($e, $ec, true), 1); // On enlève ce cours de la liste principale
					}
					else {
						$ecBug[] = array('ec' => $e, 'apsa' => $apsaBug); // On le place dans la liste des EC qui posent problème avec ses APSA
						array_splice($ec, array_search($e, $ec, true), 1); // On enlève ce cours de la liste principale
						
						$apsaBug = array(); // On réinitialise la liste
					}
				}
			}
			
				
				
			// Second tour : on fixe les EC sans choix
			foreach ($ec as $e) {
				if ((count($e->getAPSAs()) == 0 || $e->getTypeCours() != 'TP') 
					&& $e->estPlacable($semaine%2, 
						$em->getRepository('StapsCoursBundle:EC')->findByCode($e->getCoursPrecedent()))) { 
					for ($jour = 0; $jour < 7; ++$jour) {
						// On calcule la date de traitement
						$intervalleJour = $semaine * 7 + $jour;
						$dateActu = (clone $debutPeriode)->add(new \DateInterval("P".$intervalleJour."D"));
							
						// Si cette date est dépassée, on retourne l'emploi du temps actuel
						if ($dateActu > $parametres->getFinPeriode()) {
							return $edt;
						}
							
						// On ne traite pas les dimanches
						if ($dateActu->format('D') == 'Sun') {
							continue;
						}
							
						for ($creneau = 0; $creneau < $nbCreneaux; ++$creneau) {
							// On calcule l'heure de traitement
							$intervalleTemps = 
								$creneau * getdate($parametres->getDureeCreneau()->getTimestamp())['hours'] + 8;

							$heureActu = (clone $debutPeriode)
								->add(new \DateInterval("P".$intervalleJour."DT".$intervalleTemps."H"));
								
							// On ne traite pas les samedi après-midi
							if ($dateActu->format('D') == 'Sat' && getdate($heureActu->getTimestamp())['hours'] > 12) {
								break;
							}

							if (!$this->conflitParcours($e, $edt[$semaine][$jour][$creneau], 
								$e->getEffectifPrevuLicence($parametres))) {
								// On récupère les profs et on rempli la carte des places
								$profs = $em->getRepository('StapsUserBundle:Enseignant')->findByEC($e);
								$eventsParents = $em->getRepository('StapsCalendrierBundle:EventParent')->findByEnseignant($profs);

								$idProfs = $this->filtreMoment('prof', $eventsParents, $heureActu);
								$this->filtreID('prof', $idProfs, $edt[$semaine][$jour][$creneau]);

								$e->setCartePlacesCreneau($jour, $creneau, array(), $idProfs);
							}
						}
					}
				
					$nbGroupes = $e->getNbGroupes($parametres);
					$nbPlaces = $e->getNbPlaces();	

					if ($nbPlaces < $nbGroupes) {
						//echo $e->getIntitule().' EC bug ';
						$ecBug[] = array('ec' => $e); // On le place dans la liste des EC qui bloquent
						array_splice($ec, array_search($e, $ec, true), 1); // On enlève ce cours de la liste principale
					}
					else if ($nbPlaces > $nbGroupes) {
						//echo $e->getIntitule().' EC libre ';
						continue;
					}
					else {	
						//echo $e->getIntitule().' EC fixe ';
						$this->placerCreneaux($edt[$semaine], $parametres, $nbGroupes, $e);
						
						// Si on a placé toutes les heures du cours
						if ($e->estFixe()) {
							$ecFixes[] = $e; // On le place dans la liste des EC fixés
							array_splice($ec, array_search($e, $ec), 1); // On enlève ce cours de la liste principale
						}
					}
				}
			}
				

			// Troisième tour : on fixe les EC qui ont des choix
			// On fait la carte des places pour tous les EC pour chaque créneau
			$carte = 
				array_fill(0, 6, 
					array_fill(0, $nbCreneaux, array())
				);

			for ($jour = 0; $jour < 6; ++$jour) {
				for ($creneau = 0; $creneau < $nbCreneaux; ++$creneau) {
					foreach ($ec as $e) {
						if ($e->getNbPlacesCreneau($jour, $creneau) != 0
							&& $e->estPlacable($semaine%2, 
							$em->getRepository('StapsCoursBundle:EC')->findByCode($e->getCoursPrecedent()))) { 							
							// On stocke les id des EC qui peuvent se placer à ce créneau
							$carte[$jour][$creneau][] = $e->getId();
						}
					}
				}
			}
			

			foreach ($ec as $e) {
				if ($e->estPlacable($semaine%2, 
					$em->getRepository('StapsCoursBundle:EC')->findByCode($e->getCoursPrecedent()))) { 
					$nbGroupes = $e->getNbGroupes($parametres);
					$this->placerCreneaux($edt[$semaine], $parametres, $nbGroupes, $e);
					
					if ($e->estFixe()) {
						$ecFixes[] = $e; // On le place dans la liste des EC fixés
						array_splice($ec, array_search($e, $ec), 1); // On enlève ce cours de la liste principale
					}
					else {
						$ecBug[] = array('ec' => $e); // On le place dans la liste des EC qui bloquent
						array_splice($ec, array_search($e, $ec), 1); // On enlève ce cours de la liste principale
					}
				}
			}
			
			// Dernier tour des EC pour fixer de manière fictive ceux qui restent	
			foreach ($ecBug as $e) {
				if ($parametres->getToutPlacer()) { 
					if (array_key_exists('apsa', $e)) {
						foreach ($e['apsa'] as $apsa) {
							$nbGroupes = $apsa->getGroupes() - $apsa->getGroupesPlaces();
							$this->placementDefaut($edt[$semaine], $parametres, 
								$nbGroupes, $e['ec'], $apsa);
								
							// Si tous les groupes de l'APSA ont été placés (normalement toujours vrai)
							if ($apsa->getGroupesPlaces() == $nbGroupes) {
								// On supprime l'APSA de la liste		
								$apsa->setGroupesPlaces(0);					
								array_splice($e['apsa'], array_search($apsa, $e['apsa'], true), 1);
							}
						}																				
					}
					else {
						$nbGroupes = $e['ec']->getNbGroupes($parametres) - $e['ec']->getGroupesPlaces();
						$this->placementDefaut($edt[$semaine], $parametres, 
								$nbGroupes, $e['ec']);
						$e['ec']->setGroupesPlaces(0);
					}
				}	
				
				// On place l'EC dans la liste normale ou dans la liste des EC fixés
				($e['ec']->estFixe()) ? $ecFixes[] = $e['ec'] : $ec[] = $e['ec'];
				array_splice($ecBug, array_search($e, $ecBug, true), 1); // On enlève ce cours de la liste des bloquages
			}
		}
		return $edt;
		
	}
	
	/**
     * Filtre selon $filtre les enseignants ou lieux qui peuvent être choisis lors d'un créneau.
     * 
     * @param {
     *  $filtre string
     *  $eventsParents array(\Staps\CalendrierBundle\Entity\EventParent)
     *  $heureActu datetime
     * }
     * 
     * @return array(integer)
     */
	private function filtreMoment($filtre, $eventsParents, $heureActu) {
		$id = array();
		
		foreach ($eventsParents as $ep) {
			foreach ($ep->getEvents() as $e) {
				// Si la date de traitement est dans la bonne période et le bon jour
				if ($e->getDateStart() <= $heureActu && $heureActu < $e->getDateEnd()
					&& strpos($ep->getDow(),  $heureActu->format('w'))) {
					$heureDebut = 
						new \DateTime($heureActu->format('Y-m-d ').$ep->getStart()->format('H:i'));
										
					$heureFin = 
						new \DateTime($heureActu->format('Y-m-d ').$ep->getEnd()->format('H:i'));
											
					// Si l'heure de traitement est dans le bon créneau
					if ($heureDebut <= $heureActu && $heureActu < $heureFin) {
						switch ($filtre) {
							case 'prof': $id[] = $ep->getEnseignant()->getId();
							break;
							case 'lieu': $id[] = $ep->getLieu()->getId();
							break;
						}
					}
				}
			}
		}
		
		return $id;
	}
	
	/**
     * Placement des EC et APSA dans l'emploi du temps.
     * 
     * @param {
     *  $edtSemaine partie de la structure de données de l'emploi du temps
     *  $parametres \Staps\GenerateurBundle\Entity\Parametres
     *  $nbGroupes integer
     *  $ec \Staps\CoursBundle\Entity\EC
     *  $apsa \Staps\CoursBundle\Entity\APSA (facultatif)
     * }
     */
	private function placerCreneaux(&$edtSemaine, $parametres, $nbGroupes, $ec, $apsa = null) {
		($apsa != null) ? $apsa_id = $apsa->getId() : $apsa_id = null;
				
		for ($i = 0; $i < $nbGroupes; ++$i) {
			// On récupère les indices ainsi que le lieu et l'enseignant d'un créneau disponible
			$infosPlace = $this->getInfosPlaceDispo($ec, $edtSemaine);
			
			if ($infosPlace != null) {
				$edtSemaine[$infosPlace['jour']][$infosPlace['creneau']][] = 
					array('groupe' => $i + 1, 'ec' => $ec->getId(), 'apsa' => $apsa_id, 
					'lieu' => $infosPlace['lieu'], 'prof' => $infosPlace['prof'],
					'default' => $infosPlace['default']);
					
				// On ajoute les heures placées
				$heures = getdate($parametres->getDureeCreneau()->getTimestamp())['hours'];				
				($apsa != null) ? $apsa->addHeuresPlacees($heures) : $ec->addHeuresPlacees($heures);
			}
			else {
				($apsa != null) ? $apsa->setGroupesPlaces($i) : $ec->setGroupesPlaces($i);
				return null;
			}
		}	
		
		($apsa != null) ? $apsa->setGroupesPlaces($nbGroupes) : $ec->setGroupesPlaces($nbGroupes);	
	}
	
	/**
     * Récupère les informations du meilleur créneau disponible de la semaine pour un EC.
     * 
     * @param {
     *  $ec \Staps\CoursBundle\Entity\EC
     *  $edtSemaine une partie de la structure de données de l'emploi du temps
     * }
     * 
     * @return array ou null
     */
	private function getInfosPlaceDispo($ec, $edtSemaine) {	
		$carte = $ec->getCartePlaces();
		$em = $this->getDoctrine()->getManager();
		
		// On cherche le créneau qui pose le moins de problèmes
		$index = array('jour' => null, 'creneau' => null);
		$minConflit = array('lieu-prof' => INF, 'lieu' => INF, 'prof' => INF);	
		$default = false;	
		
		foreach ($carte as $jour) {
			foreach ($jour as $creneau) {
				$nbConflits = count($creneau['profs']) + count($creneau['lieux']);

				if (count($creneau['lieux']) > 0 && count($creneau['profs']) > 0) {
					if ($nbConflits < $minConflit['lieu-prof']) {
						$minConflit['lieu-prof'] = $nbConflits;
					
						$index['jour'] = array_search($jour, $carte);
						$index['creneau'] = array_search($creneau, $jour);
					}	
				}
				else if ($minConflit['lieu-prof'] == INF) {
					if (count($creneau['lieux']) > 0) {
						if ($nbConflits < $minConflit['lieu']) {
							$minConflit['lieu'] = $nbConflits;
					
							$index['jour'] = array_search($jour, $carte);
							$index['creneau'] = array_search($creneau, $jour);
						}						
					}
					else if ($minConflit['lieu'] == INF) {
						if (count($creneau['profs']) > 0) {
							if ($nbConflits < $minConflit['prof']) {
								$minConflit['prof'] = $nbConflits;
					
								$index['jour'] = array_search($jour, $carte);
								$index['creneau'] = array_search($creneau, $jour);
							}						
						}
					}
				}
			}
		}
			
		if ($index['jour'] === null || $index['creneau'] === null) {
			return null;
		}
		
		// On choisit le couple enseignant-lieu qui pose le moins de problèmes
		$id = array('prof' => null, 'lieu' => null);
		$minConflits = array('prof' => INF, 'lieu' => INF);
		
		$creneau = $carte[$index['jour']][$index['creneau']];
		$newId = array('prof' => $creneau['profs'], 'lieu' => $creneau['lieux']);

		foreach ($creneau['profs'] as $idProf) {
			$profTmp = $em->getRepository('StapsUserBundle:Enseignant')->find($idProf);
					
			if (count($profTmp->getECs()) + count($profTmp->getAPSAs()) < $minConflits['prof']) {
				$id['prof'] = $idProf;
				$minConflits['prof'] = count($profTmp->getECs()) + count($profTmp->getAPSAs());
			}
		}
		
		foreach ($creneau['lieux'] as $idLieu) {				
			$lieuTmp = $em->getRepository('StapsCoursBundle:Lieu')->find($idLieu);
					
			if (count($lieuTmp->getAPSAs()) < $minConflits['lieu']) {
				$id['lieu'] = $idLieu;
				$minConflits['lieu'] = count($lieuTmp->getAPSAs());
			}
		}
		
		if ($id['prof'] != null) {
			array_splice($carte[$index['jour']][$index['creneau']]['profs'], 
				array_search($id['prof'], $carte[$index['jour']][$index['creneau']]['profs']), 1);
		}
					
		if ($id['lieu'] != null) {
			array_splice($carte[$index['jour']][$index['creneau']]['lieux'],
				array_search($id['lieu'], $carte[$index['jour']][$index['creneau']]['lieux']), 1);			
		}
		
		$ec->setCartePlaces($carte);
		
		return array('jour' => $index['jour'], 'creneau' => $index['creneau'], 
					'prof' => ($id['prof'] == null) ? -1 : $id['prof'], 
					'lieu' => ($id['lieu'] == null) ? -1 : $id['lieu'],
					'default' => $default
		);
	}
	
	/**
     * Placement des EC et APSA dans l'emploi du temps par défaut.
     * 
     * @param {
     *  $edtSemaine partie de la structure de données de l'emploi du temps
     *  $parametres \Staps\GenerateurBundle\Entity\Parametres
     *  $nbGroupes integer
     *  $ec \Staps\CoursBundle\Entity\EC
     *  $apsa \Staps\CoursBundle\Entity\APSA (facultatif)
     * }
     */
	private function placementDefaut(&$edtSemaine, $parametres, $nbGroupes, $ec, $apsa = null) {
		($apsa != null) ? $apsa_id = $apsa->getId() : $apsa_id = null;
		
		// On fixe les creneaux obligatoires
		for ($i = 0; $i < $nbGroupes; ++$i) {
			// On récupère les indices dans $edtSemaine d'un créneau avec peu de cours qui ne génère pas de conflit
			$infosPlace = $this->getCreneauVide($ec, $edtSemaine, $parametres); 

			// On fixe ce cours au créneau récupéré avec un lieu et enseignant fictif
			$edtSemaine[$infosPlace['jour']][$infosPlace['creneau']][] = 
				array('groupe' => $i + 1, 'ec' => $ec->getId(), 'apsa' => $apsa_id, 
					'lieu' => -1, 'prof' => -1, 'default' => true);
				
			// On indique le nombre d'heures placées par groupe pour ce cours 
			$heures = getdate($parametres->getDureeCreneau()->getTimestamp())['hours'];
			($apsa != null) ? $apsa->addHeuresPlacees($heures) : $ec->addHeuresPlacees($heures);
		}
		
		($apsa != null) ? $apsa->setGroupesPlaces($nbGroupes) : $ec->setGroupesPlaces($nbGroupes);
	}
	
	/**
     * Récupère les informations du premier créneau de la semaine avec le moins de cours.
     * 
     * @param {
     *  $ec \Staps\CoursBundle\Entity\EC
     *  $edtSemaine partie de la structure de données de l'emploi du temps
     *  $parametres \Staps\GenerateurBundle\Entity\Parametres
     * }
     * 
     * @return array(integer, integer)
     */
	private function getCreneauVide($ec, $edtSemaine, $parametres) {
		$indiceJour = null;
		$indiceCreneau = null;
		$tmpPos = null;
		$pos = null;
		$minPlaces = INF;

		foreach ($edtSemaine as $jour) {
			foreach ($jour as $creneau) {
				if (!(array_search($jour, $edtSemaine) == 5 && array_search($creneau, $jour) >= count($jour)/2 - 1)) {
					if (count($creneau) < $minPlaces 
						&& !$this->conflitParcours($ec, $creneau, $ec->getEffectifPrevuLicence($parametres))) {
						$minPlaces = count($creneau);

						$indiceJour = array_search($jour, $edtSemaine);
						$indiceCreneau = array_search($creneau, $jour);
					}
				}
			}
		}
		
		return array('jour'=> $indiceJour, 'creneau' => $indiceCreneau);
	}
	
	/**
     * Verifie les conflits à un créneau particulier.
     * 
     * @param {
     *  $ec \Staps\CoursBundle\Entity\EC
     *  $creneau partie de la structure de données de l'emploi du temps
     *  $effectifMax integer
     * }
     * 
     * @return boolean
     */
	private function conflitParcours($ec, $creneau, $effectifMax) {
		$sommeTotale = $ec->getEffectifGroupe();
		$memeType = true;
		
		// On calcule le nombre d'élèves de la licence d'$ec affecté à ce créneau
		foreach ($creneau as $tuple) {
			$cours = $this->getDoctrine()->getManager()->
				getRepository('StapsCoursBundle:EC')->find($tuple['ec']);
				
			$parcoursAPlacer = explode(',', $ec->getParcours());
			$parcoursPlace = explode(',', $cours->getParcours());
			
			// Si c'est la même année et il y a des parcours en commun
			if (substr($ec->getNumero(), 0, 1) == substr($cours->getNumero(), 0, 1)
				&& !empty(array_intersect($parcoursAPlacer, $parcoursPlace))) {
				$sommeTotale += $cours->getEffectifGroupe();
				$memeType = $memeType && $ec->getTypeCours() == $cours->getTypeCours();
			}
		}
		
		return !($memeType && $sommeTotale <= $effectifMax);
	}
	
	/**
     * Récupère les informations d'un créneau disponible pour $ec dans $carte.
     * 
     * @param {
     *  $ec_id integer
     *  $carte array(integer)
     * }
     * 
     * @return array(integer, integer)
     */
	private function getCreneauDisponible($ec_id, $carte) {
		$jour = null;
		$creneau = null;
		$minPlaces = INF;

		foreach ($carte as $jour) {
			foreach ($jour as $creneau) {				
				// On vérifie si l'id que l'on cherche existe dans ce créneau et que le nombre d'ec plaçables à ce créneau
				// est le plus faible
				if (array_search($ec_id, $creneau) > 0 && count($creneau) < $minPlaces) {
					$minPlaces = count($creneau);
	
					$jour = $i;
					$creneau = $j;
				}
			}
		}

		if (!in_array(null, array($jour, $creneau))) {
			array_splice($carte[$jour][$creneau], 
				array_search($ec_id, $carte[$jour][$creneau]), 1);
		}

		return array('jour'=> $jour, 'creneau' => $creneau);
	}
	
	/**
     * Initialise les cartes des EC.
     * 
     * @param {
     *  $ec array(\Staps\CoursBundle\Entity\EC)
     *  $nbCreneaux integer
     * }
     */
	private function initialiserCartes(&$ec, $nbCreneaux) {		
		foreach ($ec as $e) {
			$e->setCartePlaces(
				array_fill(0, 6,
					array_fill(0, $nbCreneaux,
						array('lieux' => array(), 'profs' => array())
					)
				)
			);
		}
	}

	/**
     * Filtre $id selon $filtre les éléments présents dans $creneau.
     * 
     * @param {
     *  $filtre string
     *  $id array(integer)
     *  $creneau une partie de la structure de données de l'emploi du temps
     * }
     */
	private function filtreID($filtre, &$id, $creneau) { // creneau = array(array(ec, lieu, prof))
		foreach ($creneau as $cours) {
			if (($pos = array_search($cours[$filtre], $id)) >= 0) {
				array_splice($id, $pos, 1);
			}
		}
	}
	
	/**
     * Modifie le fichier json pour sauvegarder les modifications.
     * 
     * @return la vue de la page de traitement
     */
     
    /**
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function sauvegarderChangementsAction(Request $request) {
		$prof = $this->container->get('security.token_storage')->getToken()->getUser()->getEnseignant();
		
		if ($request->isXmlHttpRequest()) {
			$events = $_POST['events'];
			$edtFile = $_POST['edt'];
			
			$parametresFile = "agenda/parametres_".
				substr($edtFile, strlen("/agenda/edt_"), strlen("YYYY-MM-dd HH:mm:ii")).".json";
			
			file_put_contents(substr($edtFile, 1), $events);
			file_put_contents("debug", json_last_error_msg());
		}
		
		return $this->render('StapsGenerateurBundle:Generateur:traitement.html.twig',
				array('parametres' => json_decode(file_get_contents($parametresFile), true),
					'edt' => $edtFile, 'nom_prenom' => $prof->getNom()." ".$prof->getPrenom()));
	}
	
	/**
     * Convertit $parametres en fichier json.
     * 
     * @param {
     *  $parametres \Staps\GenerateurBundle\Entity\Parametres
     * }
     * 
     * @return string
     */
	private function parametresToJson($parametres) {
		$array = array(
			'nom' => $parametres->getNom(),
			'toutPlacer' => $parametres->getToutPlacer(),
			'dureeCreneau' => $parametres->getDureeCreneau(),
			'debutPeriode' => $parametres->getDebutPeriode(),
			'finPeriode' => $parametres->getFinPeriode(),
			'semestre' => $parametres->getSemestre(),
			'licences' => $parametres->getLicences(),
			'capaciteCM' => $parametres->getCapaciteCM(),
			'capaciteTD' => $parametres->getCapaciteTD(),
			'capaciteTP' => $parametres->getCapaciteTP(),
			'effectifPrevuL1' => $parametres->getEffectifPrevuL1(),
			'effectifPrevuL2' => $parametres->getEffectifPrevuL2(),
			'effectifPrevuL3' => $parametres->getEffectifPrevuL3(),
		);
		
		return json_encode($array);
	}
	
	/**
     * Supprime le fichier d'un emploi du temps généré.
     * 
     * @return la redirection vers la route de l'entrée des paramètres
     */
     
    /**
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function deleteEdtAction() {
		$edtFile = $_GET['edtFile'];
		
		$parametresFile = "agenda/parametres_".
			substr($edtFile, strlen("/agenda/edt_"), strlen("YYYY-MM-dd HH:mm:ii")).".json";
		
		unlink(substr($edtFile, 1));
		unlink($parametresFile);
		
		return $this->redirectToRoute("staps_generateur_entree");
	}
}
