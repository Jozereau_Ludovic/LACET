<?php
// src/Staps/AccueilBundle/Controller/AccueilController.php

namespace Staps\AccueilBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AccueilController extends Controller {
	/** 
	 * Affiche la page d'accueil de l'application.
	 *
	 * @return la vue de la page d'accueil
	 */	
    public function accueilAction() {
        return $this->render('StapsAccueilBundle:Accueil:accueil.html.twig');
    }
    
    /** 
	 * Redirige vers la page d'accueil de l'application.
	 *
	 * @return la redirection vers la route de la page d'accueil
	 */	
    public function redirectAction() {
	    return $this->redirectToRoute('staps_accueil_homepage');
    }
}
