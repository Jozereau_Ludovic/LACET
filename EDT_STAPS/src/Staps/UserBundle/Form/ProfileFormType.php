<?php
namespace Staps\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Staps\UserBundle\Form\EnseignantType;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('enseignant', EnseignantType::class,
				array(
					'label' => 'Informations personnelles',
				)
			)
		;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }

    public function getBlockPrefix()
    {
        return 'staps_user_profile_edit';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
