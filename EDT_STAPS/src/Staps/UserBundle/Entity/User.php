<?php
// src/Staps/UserBundle/Entity/User.php

namespace Staps\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Staps\UserBundle\Repository\UserRepository")
 */
class User extends BaseUser {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
  	 * @ORM\OneToOne(targetEntity="Staps\UserBundle\Entity\Enseignant", inversedBy="user", cascade={"persist"})
	 * @ORM\JoinColumn(name="enseignant", nullable=true)
  	 */
  	private $enseignant; 

    /**
     * Set enseignant
     *
     * @param \Staps\UserBundle\Entity\Enseignant $enseignant
     *
     * @return User
     */
    public function setEnseignant(\Staps\UserBundle\Entity\Enseignant $enseignant = null)
    {
        $this->enseignant = $enseignant;
        
        if ($enseignant != null) {
			$enseignant->setUser($this);
		}

        return $this;
    }

    /**
     * Get enseignant
     *
     * @return \Staps\UserBundle\Entity\Enseignant
     */
    public function getEnseignant()
    {
        return $this->enseignant;
    }
}
