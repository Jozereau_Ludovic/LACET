<?php

namespace Staps\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Enseignant
 *
 * @ORM\Table(name="enseignant")
 * @ORM\Entity(repositoryClass="Staps\UserBundle\Repository\EnseignantRepository")
 */
class Enseignant {
	/**
     * Constructor
     */
    public function __construct()
    {
        $this->ecs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->apsas = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;
    
	/**
   	 * @ORM\ManyToMany(targetEntity="Staps\CoursBundle\Entity\EC", mappedBy="enseignants")
   	 * @ORM\JoinColumn(name="ecs", nullable=true)
   	 */
  	private $ecs;
  	
	/**
   	 * @ORM\ManyToMany(targetEntity="Staps\CoursBundle\Entity\APSA", mappedBy="enseignants")
   	 * @ORM\JoinColumn(name="apsas", nullable=true)
   	 */
  	private $apsas;
  	
  	/**
  	 * @ORM\OneToOne(targetEntity="Staps\UserBundle\Entity\User", mappedBy="enseignant", cascade={"persist"})
	 * @ORM\JoinColumn(name="user", nullable=true)
  	 */
  	private $user; 


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Enseignant
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Enseignant
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Add ec
     *
     * @param \Staps\CoursBundle\Entity\EC $ec
     *
     * @return Enseignant
     */
    public function addEC(\Staps\CoursBundle\Entity\EC $ec)
    {
        $this->ecs[] = $ec;
        $ec->addEnseignant($this);

        return $this;
    }

    /**
     * Remove ec
     *
     * @param \Staps\CoursBundle\Entity\EC $ec
     */
    public function removeEC(\Staps\CoursBundle\Entity\EC $ec)
    {
        $this->ecs->removeElement($ec);
        $ec->removeEnseignant($this);
    }

    /**
     * Get ecs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getECs()
    {
        return $this->ecs;
    }

    /**
     * Add apsa
     *
     * @param \Staps\CoursBundle\Entity\APSA $apsa
     *
     * @return Enseignant
     */
    public function addAPSA(\Staps\CoursBundle\Entity\APSA $apsa)
    {
        $this->apsas[] = $apsa;
        $apsa->addEnseignant($this);

        return $this;
    }

    /**
     * Remove apsa
     *
     * @param \Staps\CoursBundle\Entity\APSA $apsa
     */
    public function removeAPSA(\Staps\CoursBundle\Entity\APSA $apsa)
    {
        $this->apsas->removeElement($apsa);
        $apsa->removeEnseignant($this);
    }

    /**
     * Get apsas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAPSAs()
    {
        return $this->apsas;
    }

    /**
     * Set user
     *
     * @param \Staps\UserBundle\Entity\Enseignant $user
     *
     * @return Enseignant
     */
    public function setUser(\Staps\UserBundle\Entity\Enseignant $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Staps\UserBundle\Entity\Enseignant
     */
    public function getUser()
    {
        return $this->user;
    }
}
