$( function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
	
	$( '.datepicker_start' ).datepicker({
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		minDate: new Date(),
			
		onSelect: function() {
			$('.datepicker_end').datepicker('option', 'minDate', $(this).val());
				
			if ($('.datepicker_end').datepicker('option', 'disabled') == false && 
			$('.datepicker_end').datepicker('option', 'minDate') > $('.datepicker_end').val()) {
				$('.datepicker_end').val($(this).val())
			}
		}
	});
	
	$('.datepicker_end' ).datepicker({
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		minDate: new Date(),
	});
	
	$('.timepicker_start').timepicker({
		timeFormat: 'HH:mm ',
		interval: 15,
		minTime: '7',
		maxTime: '19:45',
		startTime: '07:00',
		dynamic: false,
		dropdown: true,
		scrollbar: false,
		
		change: function(time) {
		    var horaire = $(this).val().split(":");
		    var minutes = parseInt(horaire[1]) + 15;
		    	
		    if (minutes == 60) {
		    	var heures = parseInt(horaire[0]) + 1;
		    		
		    	if (heures < 10) {
		    		var newHoraire = "0" + heures.toString() + ":00";
		    	}
		    	else {
		    		var newHoraire = heures.toString() + ":00";
		    	}       		
		    }
		    else {
		    	var newHoraire = horaire[0] + ":" + minutes.toString();
		    }
		    	
		    $('.timepicker_end').timepicker('option', 'minTime', newHoraire);
		    	
		    if ($('.timepicker_end').timepicker('option', 'minTime') > $('.timepicker_end').val()) {
		    	$('.timepicker_end').val(newHoraire);
		    }
		   }
	});

	$('.timepicker_end').timepicker({
		timeFormat: 'HH:mm ',
		interval: 15,
		minTime: '7',
		maxTime: '20',
		startTime: '07:00',
		dynamic: false,
		dropdown: true,
		scrollbar: false,
	});
		
	$('.choice_type').change(function(choice) {
		//var dateEnd = document.getElementsByName("staps_calendrierbundle_event[dateEnd]"); /* Ajouter les jours en fonction du cochage */
		var choiceFreq = document.getElementsByName("staps_calendrierbundle_event[eventParent][frequency]");
		for (i = 0; i < choiceFreq.length; i++) {
			if (choiceFreq[i].checked == true) {
				switch (choiceFreq[i].value) {
					case "simple":
						$('.datepicker_end').datepicker('option', 'disabled', true);
						$('.datepicker_end').datepicker('option', 'required', false);
						$('.datepicker_end').val("");
					break;
					case "hebdomadaire":
					case "mensuel":
					case "annuel":
						$('.datepicker_end').datepicker('option', 'disabled', false);
						$('.datepicker_end').datepicker('option', 'required', true);
					break;
				}
				break;
			}
		}
	});
});
