$( function() {
	$.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
	
	$( '#datepicker_start' ).datepicker({
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		minDate: new Date(),
			
		onSelect: function() {
			var date = new Date($(this).val());

			$('#datepicker_end').datepicker('option', 'minDate', new Date(date.setDate(date.getDate() + 7)));
		}
	});
	
	$('#datepicker_end' ).datepicker({
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		minDate: new Date(),
	});
});
