$(function () {
	$('#calendar-holder').fullCalendar({
		defaultView: 'agendaWeek',
        lazyFetching: true,
		timezone: ('Europe/Paris'),
		firstDay: 1,
        slotLabelFormat: 'HH:mm',
		businessHours: {
		    start: '08:00',
		    end: '20:00',
		    dow: [1, 2, 3, 4, 5, 6]
		},
		minTime: '07:00',
		maxTime: '20:00',
		slotDuration: '00:15:00',
		allDaySlot: false,
		allDayDefault: false,
		weekends: true,
		editable: false,
		slotEventOverlap: false,
		height: "auto",
		
        dayClick: function(date, jsEvent, view) { 
        	$('#popup_new').dialog('open');
    	},

    	eventClick: function(event) {
    		if (event.allDay == false) {    	
				$('#popup_event').html(
		   			"<div class=well>" +
			   			"<strong>Titre : </strong>" + event.title +
			   			"</br><strong>Date : </strong>" + event.start.format("Do MMM YYYY") +
			   			"</br><strong>Horaires :</strong> de " + event.start.format("HH:mm") + " à " + event.end.format("HH:mm") +
			   			"</br><strong>Fréquence : </strong>" + event.start.dow +
			   		"</div>"
	   			);
	   		}
	   		else {
	   			$('#popup_event').html(
		   			"<div class=well>" +
			   			"<strong>Titre : </strong>" + event.title +
			   			"</br><strong>Date : </strong>" + event.start.format("Do MMM YYYY") + " toute la journée" +
			   			"</br><strong>Fréquence : </strong>" + event.start.dow +
			   		"</div>"
	   			);
	   		}
         	$('#popup_event').data('event', event).dialog('open');
		},
    	
        eventSources: [
            {
                url: '/full-calendar/load',
                type: 'POST',
                data: {},
                error: function () {}
            },
			{
				url: $('#dispoFile').val(),
			}
        ],
               
        eventRender: function eventRender(event, element, view) {
			// On affiche les events normalement si l'on a pas de filtre
			if (document.getElementById("prof_selector") == null) {
				return event.start.isBefore(event.dateEnd) && event.end.isAfter(event.dateStart);
        	}
        	else if (event.start.isBefore(event.dateEnd) && event.end.isAfter(event.dateStart)) {
				if (event.className == 'lieu') {
					return ($('#localite_selector').val() == 'all'
						|| event.title.indexOf($('#localite_selector').val()) >= 0)
						&& ($('#installation_selector').val() == 'all'
						|| event.title.indexOf($('#installation_selector').val()) >= 0)
						&& ($('#activite_selector').val() == 'all'
						|| event.title.indexOf($('#activite_selector').val()) >= 0);
				}
				else {
					return ['all', event.user_id].indexOf($('#prof_selector').val()) >= 0;
				}
			}
			else return false;
        },
        
        eventMouseover: function (data, event, view) {			
			tooltip = '<div class="tooltiptopicevent"'
				+ 'style="width:auto; height:auto; background:#17c4ad; '
				+ 'border:{width:10px; color:black}; position:absolute; z-index:10001; '
				+ 'padding:10px 10px 10px 10px;  line-height: 200%;">' 
				+ 'Titre : ' + data.title + '</br>'
				+ '</div>';

			$("body").append(tooltip);
			
			$(this).mouseover(function (e) {
				$(this).css('z-index', 10000);
				$('.tooltiptopicevent').fadeIn('500');
				$('.tooltiptopicevent').fadeTo('10', 1.9);
			}).mousemove(function (e) {
				$('.tooltiptopicevent').css('top', e.pageY + 10);
				$('.tooltiptopicevent').css('left', e.pageX + 20);
			});


		},
		eventMouseout: function (data, event, view) {
			$(this).css('z-index', 8);

			$('.tooltiptopicevent').remove();
		}
    });
        
    $('#popup_new').dialog({
    	title: 'Ajouter un évènement',
   		autoOpen: false,
      	height: 'auto',
      	width: 'auto',
      	modal: true,
      	resizable: false,
      	position: {
      		my: "center",
   			at: "center",
   			of: window
   		},
      	open : function(event, ui) { 
      		originalContent = $("#popup_new").html();
   		},
   		close : function(event, ui) {
      		$("#popup_new").html(originalContent);
   		},
    });
    
    $('#popup_event').dialog({
    	title: "Informations",
   		autoOpen: false,
      	height: 'auto',
      	width: 'auto',
      	modal: true,
      	resizable: false,
      	position: {
      		my: "center",
   			at: "center",
   			of: window
   		},
   		buttons: {
   			/*"Modifier": function() {
   				$('#popup_new').dialog('open');
   			},*/
   			"Supprimer": function() {
   				$('#popup_delete').dialog('open');
   			}
   		},
    });
    
    $('#popup_delete').dialog({
    	title: "Suppression",
   		autoOpen: false,
      	height: 'auto',
      	width: 'auto',
      	modal: true,
      	resizable: false,
      	position: {
      		my: "center",
   			at: "center",
   			of: window
   		},
   		buttons: {
   			"Annuler": function() {
   				$('#popup_delete').dialog('close');
   			},
   			"Supprimer": function() {
   				var type = document.getElementsByName("type");
				for (i = 0; i < type.length; i++) {
					if (type[i].checked == true) {
						var valeur = type[i].value;
						break;
					}
				}

   				$.ajax({
       				url: '/agenda/supprimer',
       				data: 'id=' + $('#popup_event').data('event').id + 
       					'&date=' + $('#popup_event').data('event').start.format("YYYY-MM-Do") +
       					'&type=' + valeur,
       				type: "POST",
       				success: function () {
          				$('#calendar-holder').fullCalendar('removeEvents', 
          				function(event) {
          					return (event.event_parent_id == $('#popup_event').data('event').event_parent_id) &&
          						(valeur == "tous"
          						||
          						(valeur == "un" && event.start == $('#popup_event').data('event').start));
          				})
       				}
  				});
  				$(this).dialog('close');
  				$("#popup_event").dialog('close');
   			}
   		},
   		open : function(event, ui) { 
      		originalContent = $("#popup_delete").html();
   		},
   		close : function(event, ui) {
      		$("#popup_delete").html(originalContent);
   		},
    });

   	
   	$(window).resize(function() {
    	$("#popup_new").dialog("option", "position", {my: "center", at: "center", of: window});
    	$("#popup_event").dialog("option", "position", {my: "center", at: "center", of: window});
    	$("#popup_delete").dialog("option", "position", {my: "center", at: "center", of: window});
	});
	
	$(window).scroll(function() {
    	$("#popup_new").dialog("option", "position", {my: "center", at: "center", of: window});
    	$("#popup_event").dialog("option", "position", {my: "center", at: "center", of: window});
    	$("#popup_delete").dialog("option", "position", {my: "center", at: "center", of: window});
	});
	
	$('#prof_selector').change(function() {
		$('#calendar-holder').fullCalendar('rerenderEvents');
	});
	
	$('#localite_selector').change(function() {
		$('#calendar-holder').fullCalendar('rerenderEvents');
	});
	
	$('#installation_selector').change(function() {
		$('#calendar-holder').fullCalendar('rerenderEvents');
	});
	
	$('#activite_selector').change(function() {
		$('#calendar-holder').fullCalendar('rerenderEvents');
	});
});
