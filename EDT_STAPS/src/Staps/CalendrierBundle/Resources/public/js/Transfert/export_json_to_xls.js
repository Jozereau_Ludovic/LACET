$(function() {
	eventTypes = {
		"id": "Number",
		"user_id": "Number",
		"className": "String",
		"title": "String",
		"start": "String",
		"end": "String",
		"dow": "String",
		"allDay": "String",
		"frequency": "String",
		"event_parent_id": "Number",
		"dateStart": "String",
		"dateEnd": "String"
	};
	
	function emitXmlHeader() {
	    var headerRow =  '<ss:Row>\n';
	    for (var colName in eventTypes) {
			headerRow += '  <ss:Cell>\n';
			headerRow += '    <ss:Data ss:Type="String">';
			headerRow += colName + '</ss:Data>\n';
			headerRow += '  </ss:Cell>\n';        
		}
		
	    headerRow += '</ss:Row>\n';    
	    return '<?xml version="1.0"?>\n' +
		   '<ss:Workbook xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">\n' +
		   '<ss:Worksheet ss:Name="Sheet1">\n' +
		   '<ss:Table>\n\n' + headerRow;
	};

	function emitXmlFooter() {
	    return '\n</ss:Table>\n' +
		   '</ss:Worksheet>\n' +
		   '</ss:Workbook>\n';
	};

	function jsonToSsXml(jsonObject) {
	    var row;
	    var col;
	    var xml;
	    var data = typeof jsonObject != "object" 
		     ? JSON.parse(jsonObject) 
		     : jsonObject;

	    xml = emitXmlHeader();

	    for (row = 0; row < data.length; row++) {
		xml += '<ss:Row>\n';

			for (col in data[row]) {
				xml += '  <ss:Cell>\n';
				xml += '    <ss:Data ss:Type="' + eventTypes[col]  + '">';
				xml += data[row][col] + '</ss:Data>\n';
				xml += '  </ss:Cell>\n';
			}

			xml += '</ss:Row>\n';
	    }

	    xml += emitXmlFooter();
	    return xml;  
	};
	
	var buttonExport = document.getElementById('export');
	
	try {
		buttonExport.onclick = function() {
			$.getJSON("disponibilites.json", function(data) {
				$('#csv').text(jsonToSsXml(data));
			});
		};
	}
	catch (err) {}
	
	function downloadFile(fileContent) {
		$.ajax({
			url: '/agenda/exporter',
			data: 'fileContent=' + fileContent,
			type: 'GET',
			success: function() {
				$('#telecharger').text("Télécharger le fichier");
				$('#telecharger').attr('href', "/agenda/telecharger");
			}
		});
	}	
});	
	
