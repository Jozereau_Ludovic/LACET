<?php
//src/Staps/CalendrierBundle/EventListener/CalendrierEventListener.php

namespace Staps\CalendrierBundle\EventListener;

use Staps\CalendrierBundle\Entity\Event;
use AncaRebeca\FullCalendarBundle\Event\CalendarEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CalendrierEventListener {
	private $entityManager;
	private $container;
	
	public function __construct(EntityManager $entityManager, ContainerInterface $container) {
		$this->entityManager = $entityManager;
		$this->container = $container;
	}

	public function loadData(CalendarEvent $calendarEvent) {
		$startDate = $calendarEvent->getStart();
		$endDate = $calendarEvent->getEnd();
		$filters = $calendarEvent->getFilters();
		
		$user = $this->container->get('security.token_storage')->getToken()->getUser();

		// On supprime tous les event_parent sans fils
		$this->entityManager->getRepository('StapsCalendrierBundle:EventParent')->deleteNoChild();
		
		if ($this->container->get('security.authorization_checker')->isGranted('ROLE_RESPONSABLE')) {
			$results = $this->entityManager->getRepository('StapsCalendrierBundle:EventParent')
				->findAll();
		}
		else {
			$prof = $user->getEnseignant();
			
			$results = $this->entityManager->getRepository('StapsCalendrierBundle:EventParent')
				->findBy(array('enseignant' => $prof, 'className' => 'enseignant'));
		}
		
		$rows = array();
		foreach ($results as $eventParent) {
			if (!in_array(null, array($eventParent->getStart(), $eventParent->getEnd()))) {
				$event_parent_id = strval($eventParent->getId());
				$start = $eventParent->getStart()->format('H:i:s');
				$end = $eventParent->getEnd()->format('H:i:s');
				$frequency = $eventParent->getFrequency();
				$dow = $eventParent->getDow();
				$className = $eventParent->getClassName();
				$title = str_replace("\\n", "\n", $eventParent->getTitle());
				
				if ($eventParent->getEnseignant() != null) {
					$prof_id = strval($eventParent->getEnseignant()->getId());
				}
				else {
					$prof_id = null;
				}
				
				if ($eventParent->getAllDay() == 0) {
					$allDay = false;
				}
				else {
					$allDay = true;
					$start = null;
					$end = null;
				}
				
				foreach ($eventParent->getEvents() as $event) {
					$id = strval($event->getId());
					$dateStart = $event->getDateStart()->format('Y-m-d');
					$dateEnd = $event->getDateEnd()->format('Y-m-d');
					
					$rows[] = array(
						'id' => $id, 'user_id' => $prof_id, 'className' => $className, 
						'title' => $title, 'start' => $start, 'end' => $end, 
						'dow' => $dow, 'allDay' => $allDay, 'frequency' => $frequency, 
						'event_parent_id' => $event_parent_id, 'dateStart' => $dateStart, 
						'dateEnd' => $dateEnd
					);
				}
			}
		}
		
		file_put_contents("agenda/disponibilites_user".$user->getId().".json", json_encode($rows));
		file_put_contents("debug_dispo", json_last_error_msg());
    }
}
