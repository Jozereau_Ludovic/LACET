<?php

namespace Staps\CalendrierBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="Staps\CalendrierBundle\Repository\EventRepository")
 */
class Event {
	/**
	 * @ORM\ManyToOne(targetEntity="Staps\CalendrierBundle\Entity\EventParent", inversedBy="events", cascade={"persist"})
 	 * @ORM\JoinColumn(nullable=false)
	 */
	private $eventParent;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStart", type="date", nullable=true)
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateEnd", type="date", nullable=true, nullable=true)
     */
    private $dateEnd;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return Event
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Event
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set eventParent
     *
     * @param \Staps\CalendrierBundle\Entity\EventParent $eventParent
     *
     * @return Event
     */
    public function setEventParent(\Staps\CalendrierBundle\Entity\EventParent $eventParent)
    {
		if ($this->eventParent != null) {
			$this->eventParent->removeEvent($this);
		}
		
        $this->eventParent = $eventParent;
        $eventParent->addEvent($this);

        return $this;
    }

    /**
     * Get eventParent
     *
     * @return \Staps\CalendrierBundle\Entity\EventParent
     */
    public function getEventParent()
    {
        return $this->eventParent;
    }
}
