<?php

namespace Staps\CalendrierBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * EventParent
 *
 * @ORM\Table(name="event_parent")
 * @ORM\Entity(repositoryClass="Staps\CalendrierBundle\Repository\EventParentRepository")
 */
class EventParent {
	/**
     * Constructor
     */
    public function __construct()
    {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }

	/**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
	/**
	 * @ORM\OneToMany(targetEntity="Staps\CalendrierBundle\Entity\Event", mappedBy="eventParent", cascade={"persist", "remove"})
 	 * @ORM\JoinColumn(nullable=true)
	 */
	private $events;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Staps\UserBundle\Entity\Enseignant", cascade={"persist"})
	 */
	private $enseignant;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Staps\CoursBundle\Entity\Lieu")
	 */
	private $lieu;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="className", type="string", length=255)
	 */
	private $className;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="time", nullable=true)
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="time", nullable=true)
     */
    private $end;

    /**
     * @var string
     *
     * @ORM\Column(name="dow", type="string", length=255, nullable=true)
     */
    private $dow;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="allDay", type="boolean")
     */
    private $allDay = false;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="frequency", type="string", length=255)
     */
    private $frequency = 0;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set className
     *
     * @param string $className
     *
     * @return EventParent
     */
    public function setClassName($className)
    {
        $this->className = $className;

        return $this;
    }

    /**
     * Get className
     *
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return EventParent
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return EventParent
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     *
     * @return EventParent
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set dow
     *
     * @param string $dow
     *
     * @return EventParent
     */
    public function setDow($dow)
    {
        $this->dow = $dow;

        return $this;
    }

    /**
     * Get dow
     *
     * @return string
     */
    public function getDow()
    {
        return $this->dow;
    }

    /**
     * Set allDay
     *
     * @param boolean $allDay
     *
     * @return EventParent
     */
    public function setAllDay($allDay)
    {
        $this->allDay = $allDay;

        return $this;
    }

    /**
     * Get allDay
     *
     * @return boolean
     */
    public function getAllDay()
    {
        return $this->allDay;
    }

    /**
     * Set frequency
     *
     * @param string $frequency
     *
     * @return EventParent
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return string
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Add event
     *
     * @param \Staps\CalendrierBundle\Entity\Event $event
     *
     * @return EventParent
     */
    public function addEvent(\Staps\CalendrierBundle\Entity\Event $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \Staps\CalendrierBundle\Entity\Event $event
     */
    public function removeEvent(\Staps\CalendrierBundle\Entity\Event $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Set enseignant
     *
     * @param \Staps\UserBundle\Entity\Enseignant $enseignant
     *
     * @return EventParent
     */
    public function setEnseignant(\Staps\UserBundle\Entity\Enseignant $enseignant = null)
    {
        $this->enseignant = $enseignant;

        return $this;
    }

    /**
     * Get enseignant
     *
     * @return \Staps\UserBundle\Entity\Enseignant
     */
    public function getEnseignant()
    {
        return $this->enseignant;
    }

    /**
     * Set lieu
     *
     * @param \Staps\CoursBundle\Entity\Lieu $lieu
     *
     * @return EventParent
     */
    public function setLieu(\Staps\CoursBundle\Entity\Lieu $lieu = null)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return \Staps\CoursBundle\Entity\Lieu
     */
    public function getLieu()
    {
        return $this->lieu;
    }
}
