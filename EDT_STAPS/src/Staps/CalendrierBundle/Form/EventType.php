<?php

namespace Staps\CalendrierBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Staps\CalendrierBundle\Form\EventParentType;

use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class EventType extends AbstractType {
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
        	->add('eventParent', EventParentType::class,
        		array(
        			'label' => false,
        		)
        	)
        	->add(
        		'dateStart', DateType::class, 
        		array(
        			'html5' => false,
		    		'widget' => 'single_text',
		    		'attr' => array('class' => 'js-datepicker'), 
		    		'label' => 'Début de la période',
        		)
        	)
        	->add('dateEnd', DateType::class, 
        		array(
        			'html5' => false,
		    		'widget' => 'single_text',
		    		'attr' => array('class' => 'js-datepicker'), 
		    		'label' => 'Fin de la période (non incluse)',
		    		'required' => false,
        		)
        	)
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Staps\CalendrierBundle\Entity\Event'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'staps_calendrierbundle_event';
    }


}
