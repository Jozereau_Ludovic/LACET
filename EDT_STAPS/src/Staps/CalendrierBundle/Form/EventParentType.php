<?php

namespace Staps\CalendrierBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EventParentType extends AbstractType {
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
        	->add('start', TimeType::class,
        		array(
        			'html5' => false,
		    		'widget' => 'single_text',
		    		'label' => 'Heure de début',
		    	)
		    )
        	->add('end', TimeType::class, 
        		array(
        			'html5' => false,
		    		'widget' => 'single_text',
		    		'label' => 'Heure de fin',
		    	)
		    )
        	->add('dow', ChoiceType::class,
        		array(
        			'choices' => array(
        				'Lundi' => 1,
        				'Mardi' => 2,
        				'Mercredi' => 3,
        				'Jeudi' => 4,
        				'Vendredi' => 5,
        				'Samedi' => 6
        			),
         			'expanded' => true,
		        	'multiple' => true,
		        	'label' => 'Jours',
		        )
		    )
        	->add('allDay', CheckboxType::class,
        		array(
        			'required' => false,
        			'label' => 'Toute la journée',
        		)
        	)
        	->add('frequency', ChoiceType::class, 
		    	array(
					'choices' => array(
		        		'Simple' => "simple",
		        		'Hebdomadaire' => "hebdomadaire",
		        		'Mensuel' => "mensuel",
		        		'Annuel' => "annuel",
		        	),
		        	'expanded' => true,
		        	'multiple' => false,
		        	'label' => 'Fréquence',
		        )
		    )
		    ->add('submit', SubmitType::class,
		    	array(
		    		'label' => 'Valider',
		    	)
		    )
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Staps\CalendrierBundle\Entity\EventParent'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'staps_calendrierbundle_eventparent';
    }


}
