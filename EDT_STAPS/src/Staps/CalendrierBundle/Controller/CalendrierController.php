<?php

namespace Staps\CalendrierBundle\Controller;

set_include_path(implode(PATH_SEPARATOR, [
    realpath(__DIR__ . '/../../Classes'), // assuming Classes is in the same directory as this script
    get_include_path()
]));
require_once 'PHPExcel/IOFactory.php';
require_once 'PHPExcel/Shared/Date.php';

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Staps\CalendrierBundle\Entity\Event;
use Staps\CalendrierBundle\Form\EventType;
use Staps\CalendrierBundle\Entity\EventParent;
use Staps\CalendrierBundle\Form\EventParentType;
use Staps\CoursBundle\Entity\Lieu;
use Staps\UserBundle\Entity\Enseignant;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class CalendrierController extends Controller {
	/**
	 * Charge les événements de la base de données pour les afficher sur l'agenda.
	 * Permet le chargement du formulaire d'ajout d'événements et des informations de filtre.
	 * Méthode appelée à chaque changement sur l'agenda.
	 *
	 * @param {
	 *  $request Request
	 * }
	 * 
	 * @return la vue de l'agenda avec le formulaire et les filtres
	 */
	 
	/**
	 * @Security("has_role('ROLE_USER')")
	 */
	public function loadAction(Request $request) {
		$startDate = new \DateTime($request->get('start'));
        $endDate = new \DateTime($request->get('end'));
        $filters = $request->get('filters', []);
        
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
       
        try {
            $content = $this
                ->get('anca_rebeca_full_calendar.service.calendar')
                ->getData($startDate, $endDate, $filters);
            $status = empty($content) ? Response::HTTP_NO_CONTENT : Response::HTTP_OK;
        } catch (\Exception $exception) {
            $content = json_encode(array('error' => $exception->getMessage()));
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        }
        
		// Création du formulaire d'ajout d'event
		$event = new Event();
		$form = $this->createForm(EventType::class, $event);
		
		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
			$prof = $user->getEnseignant();
						
			$event->getEventParent()->setEnseignant($prof);
			$event->getEventParent()->setTitle($prof->getNom()." ".$prof->getPrenom());
			$event->getEventParent()->setClassName("enseignant");
			$dow = "[";
			foreach($event->getEventParent()->getDow() as $ev) {
				$dow = $dow.$ev.", ";
			}
			$dow = $dow."]";
			$dow = str_replace(", ]", "]", $dow);
			$event->getEventParent()->setDow($dow);
			
			if ($event->getEventParent()->getFrequency() == "simple") {
				$day = clone $event->getDateStart();
				$event->setDateEnd($day->modify("+1 day"));
			}
						
			$em->persist($event);
			$em->flush();
			
			$event = new Event();
			$form = $this->createForm(EventType::class, $event);
		}
		
		$profs = $em->getRepository('StapsUserBundle:Enseignant')->findAll();
		usort($profs, array($this, "tri_alphabetique"));
		
		$eventLieux = $em->getRepository('StapsCalendrierBundle:EventParent')->findByClassName('lieu');
		
		$localites = array();
		$installations = array();
		$activites = array();
		foreach ($eventLieux as $eventLieu) {
			$lieu = $eventLieu->getLieu();
			
			$localites[] = $lieu->getLocalite();
			$installations[] = $lieu->getInstallation();
			$activites[] = $lieu->getZoneActivite();
		}
		$localites = array_unique($localites);
		$installations = array_unique($installations);
		$activites = array_unique($activites);
		
		asort($localites);
		asort($installations);
		asort($activites);

        return $this->render('StapsCalendrierBundle:Calendrier:agendaDisponibilites.html.twig', 
			array('form' => $form->createView(), 'profs' => $profs, 'localites' => $localites, 
			'installations' => $installations, 'activites' => $activites, 
			'dispo' => "/agenda/disponibilites_user".$user->getId().".json"));
    }
    
    /**
	 * Méthode de tri des enseignants par ordre alphabétique
	 * 
	 * @param {
	 * 	$a \Staps\UserBundle\Entity\Enseignant
	 * 	$b \Staps\UserBundle\Entity\Enseignant
	 * }
	 * 
	 * @return integer
	 */ 
	private function tri_alphabetique(\Staps\UserBundle\Entity\Enseignant $a, \Staps\UserBundle\Entity\Enseignant $b) {
		$nom = 0;
		
		$a->getNom() < $b->getNom() ? $nom += -1 : $nom += 1;

		if ($nom != 0) {
			return $nom;
		}
		
		$prenom = 0;
		
		$a->getPrenom() < $b->getPrenom() ? $prenom += -1 : $prenom += 1;
		
		return $prenom;
	}
    
    /**
     * Permet la suppression d'un ou plusieurs événements de la base de données
     *
     * @param {
     *  $request Request
     * }
     * 
     * @return la redirection vers la route de l'agenda
     */
     
    /**
	 * @Security("has_role('ROLE_USER')")
	 */
    public function supprimerEventAction(Request $request) {
   		if ($request->isXmlHttpRequest()) {
   			$id = $_POST['id'];
   			$type = $_POST['type'];
   			   			
   			// On récupère le repository des event
   			$repository = $this->getDoctrine()->getManager()->getRepository('StapsCalendrierBundle:Event');

			// On récupère l'évènement par son id dans la bdd
			$event = $repository->find($id);
			
			if ($type == 'un') {
				$date = new \Datetime($_POST['date']);
				
				// Si l'évènement est le premier de la période
				if ($date == $event->getDateStart()) {
					$event->setDateStart(clone $date->modify("+1 day")); // On démarre la période plus tard
				}
				// Si l'évènement est le dernier de la période
				else if ($date == clone $event->getDateEnd()->modify("-1 day")) {
					$event->setDateEnd($date); // On termine la période plus tôt
				}
				// Sinon, l'évènement est dans la période, on doit créer deux sous-évènements
				else {
					$newDateEnd = clone $date;
					
					$sousEvent = clone $event;
					$sousEvent->setDateEnd($newDateEnd);
					
					$event->setDateStart($date->modify("+1 day"));
					
					$this->getDoctrine()->getManager()->persist($sousEvent);
				}
			}
			else {
				$events = $repository->findEventByParent($event->getEventParent()->getId());
				
				foreach ($events as $e) {
					$this->getDoctrine()->getManager()->remove($e);
				}
			}
						
			// On supprime de la bdd tous les event impossibles
			$this->getDoctrine()->getManager()->getRepository('StapsCalendrierBundle:Event')->deleteImpossible();

			// Puis on supprime tous les event_parent sans fils
			$this->getDoctrine()->getManager()->getRepository('StapsCalendrierBundle:EventParent')->deleteNoChild();	
			
			$this->getDoctrine()->getManager()->flush();
    	}
    	
    	return $this->redirectToRoute('staps_calendrier_gererDisponibilites');
    }
    
    /**
     * Fournit une tête de fichier xml à partir du tableau des titres et types voulus.
     * 
     * @param {
     *  $eventTypes array(key => value)
     * }
     * 
     * @return string
     */
    private function emitXmlHeader($eventTypes) {
    	$xml = "<ss:Row>\n";
	    	    
	    foreach ($eventTypes as $key => $colName) {
			$xml = $xml."  <ss:Cell>\n";
			$xml = $xml."    <ss:Data ss:Type=\"String\">";
			$xml = $xml.$key."</ss:Data>\n";
			$xml = $xml."  </ss:Cell>\n";        
		}
		
	    $xml = $xml."</ss:Row>\n";    
	   
	    return "<?xml version=\"1.0\"?>\n".
		   "<ss:Workbook xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\">\n".
		   "<ss:Worksheet ss:Name=\"Sheet1\">\n".
		   "<ss:Table>\n\n".$xml;
	}

	/**
     * Fournit une pied de fichier xml.
     * 
     * @return string
     */
	private function emitXmlFooter() {
	    return "\n</ss:Table>\n".
		   "</ss:Worksheet>\n".
		   "</ss:Workbook>\n";
	}
	
	/**
     * Convertit un fichier json en fichier xml. A supprimer avec utilisation de 
     * 
     * @param {
     *  $json string
     *  $eventTypes array(key => value)
     * }
     * 
     * @return string
     */
    private function jsonToXml($json, $eventTypes) {
	    $xml = $this->emitXmlHeader($eventTypes);
	    
	    $data = json_decode($json, true);

	    foreach ($data as $row) {
		$xml = $xml."<ss:Row>\n";
		

			foreach ($row as $key => $col) {
				$xml = $xml."  <ss:Cell>\n";
				$xml = $xml."    <ss:Data ss:Type=\"".$eventTypes[$key]."\">";
				$xml = $xml.$col."</ss:Data>\n";
				$xml = $xml."  </ss:Cell>\n";
			}

			$xml = $xml."</ss:Row>\n";
	    }

	    $xml = $xml.$this->emitXmlFooter();
	    return $xml;  
	}
    
    /**
     * Télécharge le fichier d'export des événements sous format xml à partir du fichier json.
     * 
     * @return Response
     */
     
    /**
	 * @Security("has_role('ROLE_ADMIN')")
	 */
    public function downloadAction() {   
    	$eventTypes = array(
			"id" => "Number",
			"user_id" => "Number",
			"className" => "String",
			"title" => "String",
			"start" => "String",
			"end" => "String",
			"dow" => "String",
			"allDay" => "String",
			"frequency" => "String",
			"event_parent_id" => "Number",
			"dateStart" => "String",
			"dateEnd" => "String"
		);
    
    	$json = file_get_contents('agenda/disponibilites.json');
		file_put_contents('transferts/exports/export_event.xls', $this->jsonToXml($json, $eventTypes));    	
    
    	// On créé la réponse pour télécharger le fichier d'export
   		$response = new Response();
   		$response->setContent(file_get_contents('transferts/exports/export_event.xls'));
   		$response->headers->set('Content-Type', 'application/force-download');
   		$response->headers->set('Content-disposition', 'filename=disponibilites.xls');
         
   		return $response;
    }
    

     /**
     * Récupère la colonne de la cellule contenant $value.
     * 
     * @param {
     *  $feuille feuille
     *  $value string
     * }
     * 
     * @return column ou null
     */
    private function getColumnByValue($feuille, $value) {
    	foreach ($feuille->getRowIterator() as $ligne) {
    		foreach ($ligne->getCellIterator() as $cellule) {	
				if (stripos($cellule->getCalculatedValue(), $value) > -1) {
					return $cellule->getColumn();
				}
			}
		}
		
		return null;
	}	
    
    
	/**
     * Met à jour les informations concernant les lieux dans la base de données.
     * 
     * @param {
     *  $file string
     * }
     */
    
    /**
	 * @Security("has_role('ROLE_ADMIN')")
	 */
    private function importerLieux($file) {		
		$em = $this->getDoctrine()->getManager();
		$PHPExcel = \PHPExcel_IOFactory::load($file);
		
		if (strtolower($PHPExcel->getSheet()->getCell("A1")->getValue()) == "localité") {
			//On supprime tous les événements de lieu
			$em->getRepository('StapsCalendrierBundle:EventParent')->deleteByClassName('lieu');
			$lieux = $em->getRepository('StapsCoursBundle:Lieu')->findAll();
			foreach ($lieux as $l) {
				$em->remove($l);
			}
		
			$em->flush();

			foreach ($PHPExcel->getAllSheets() as $feuille) {
				// On récupère les colonnes concernant les informations sur les lieux
				$colLocalite = $this->getColumnByValue($feuille, 'Localité');
				$colInstall = $this->getColumnByValue($feuille, 'Installation');
				$colZone = $this->getColumnByValue($feuille, 'activité');
				$colCode = $this->getColumnByValue($feuille, 'Code final');
				$colCapacite = $this->getColumnByValue($feuille, 'Capacité');
				$colJour = $this->getColumnByValue($feuille, 'Jour souhaité');
				$colHoraires = $this->getColumnByValue($feuille, 'Horaires');
				$colDebut = $this->getColumnByValue($feuille, 'Début période');
				$colFin = $this->getColumnByValue($feuille, 'Fin période');	

				// On vérifie que la feuille possède toutes les informations nécessaires
				if (!(in_array(null, array($colLocalite, $colInstall, $colZone, $colCode, $colCapacite, 
					$colJour, $colHoraires, $colDebut, $colFin)))) {
						
					for ($ligne = 2; $ligne <= $feuille->getHighestRow(); ++$ligne) {					
						if (($lieu = $em->getRepository('StapsCoursBundle:Lieu')
							->findOneByCode($feuille->getCell($colCode.$ligne)->getCalculatedValue())) == null) {
							$lieu = new Lieu();
							
							$lieu->setLocalite($feuille->getCell($colLocalite.$ligne)->getValue());
							$lieu->setInstallation($feuille->getCell($colInstall.$ligne)->getValue());
							$lieu->setZoneActivite($feuille->getCell($colZone.$ligne)->getValue());
							$lieu->setCode($feuille->getCell($colCode.$ligne)->getCalculatedValue());
							$lieu->setCapacite($feuille->getCell($colCapacite.$ligne)->getCalculatedValue());
							
							if (!in_array(null, array($lieu->getLocalite(), 
								$lieu->getInstallation(), $lieu->getZoneActivite(), $lieu->getCode()))) {
								$em->persist($lieu);
							}
							else {
								continue;
							}
						}

						$event = new Event();
						$event->setEventParent(new EventParent());
						
						$debut = date('Y-m-d', \PHPExcel_Shared_Date::ExcelToPHP($feuille->getCell($colDebut.$ligne)->getValue()));
						$fin = date('Y-m-d', \PHPExcel_Shared_Date::ExcelToPHP($feuille->getCell($colFin.$ligne)->getValue()));
						
						$jour = $feuille->getCell($colJour.$ligne)->getValue();
						$horaires = $feuille->getCell($colHoraires.$ligne)->getValue();
						
						$event->getEventParent()->setTitle('Localité : '.$lieu->getLocalite().
						'\nInstallation : '.$lieu->getInstallation().'\nZone d\'activité : '.$lieu->getZoneActivite());
						$event->getEventParent()->setClassName("lieu");
						$event->getEventParent()->setLieu($lieu);
						$event->getEventParent()->setDow($this->formaterPeriode($jour));
						$event->getEventParent()->setStart($this->formaterHeure(" à ", $horaires));
						$event->getEventParent()->setEnd($this->formaterHeure(" à ", $horaires, false));
						$event->getEventParent()->setAllDay(false);
						$event->getEventParent()->setFrequency('hebdomadaire');
						$event->setDateStart($this->formaterDate($debut));
						$event->setDateEnd($this->formaterDate($fin, false));
						
						$em->persist($event);
						$em->flush();
					}
				}
			}
		}
		else {
			$this->addFlash(
						'erreur',
						"Le fichier ne correspond pas au fichier demandé."
					);
		}
	}
	
	
	/**
     * Formate les jours données en tableau d'entiers.
     * 
     * @param {
     *  $jours string
     * }
     * 
     * @return array ou null
     */
	private function formaterPeriode($jours) {
		if ($jours != null) {
			$jours = strtolower($jours);
			
			$dow = '[';
			foreach (explode(", ", $jours) as $jour) {
				switch ($jour) {
					case "lundi": $dow = $dow."1,";
					break;
					case "mardi": $dow = $dow."2,";
					break;
					case "mercredi": $dow = $dow."3,";
					break;
					case "jeudi": $dow = $dow."4,";
					break;
					case "vendredi": $dow = $dow."5,";
					break;
					case "samedi": $dow = $dow."6,";
					break;
				}
			}
			$dow = $dow."]";
			$dow = str_replace(",]", "]", $dow);
			
			return $dow;
		}
		return null;
	}
	
	/**
     * Formate l'heure en HH:MM:00.
     * 
     * @param {
     *  $delemiteur string
     *  $heure string
     *  $debut boolean
     * }
     * 
     * @return \Datetime ou null
     */
	private function formaterHeure($delimiteur, $heure, $debut = true) {
		if ($heure != null) {
			($debut) ? $i = 0 : $i = 1;			
			$HM = explode($delimiteur, $heure)[$i];
			
			list($H, $M) = explode("h", strtolower($HM));
			if ($M == "") $M = "00";
			
			return new \Datetime($H.':'.$M.':00');
		}
		return null;
	}
	
	/**
     * Formate la date et ajoute un jour si c'est la date de fin de période.
     * 
     * @param {
     *  $date string
     *  $debut boolean
     * }
     * 
     * @return \Datetime ou null
     */
	private function formaterDate($date, $debut = true) {
		if ($date != null) {
			$dateFormate = new \Datetime($date);
			
			if (!$debut) {
				$dateFormate->modify("+1 day");
			}
			
			return $dateFormate;
		}
		return null;
	}
	
    /**
     * Importe le fichier des lieux et met à jour la base de données.
     * 
     * @return la redirection vers la route de l'agenda
     */
				
	/**
	 * @Security("has_role('ROLE_ADMIN')")
	 */			
    public function uploadAction() {
    	$extensions_valides = array('xml', 'ods', 'xls'); // Array si d'autres sont acceptées
    	$extension_upload = strtolower(substr(strrchr($_FILES['file']['name'], '.'), 1));

		if (in_array($extension_upload,$extensions_valides)) {	
			$destination_path = getcwd()."/transferts/imports/";
		   	$result = 0;
		   	$target_path = $destination_path.basename($_FILES['file']['name']);
		 
		   	if (@move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
			  	$result = 1;
		   	}
		   	
		   	$this->importerLieux($target_path);
		}
		else {
			$this->addFlash(
						'erreur',
						"Le fichier n'est pas du bon type."
					);
		}
	   	return $this->redirectToRoute('staps_calendrier_gererDisponibilites');	   	
    }
    
    /**
     * Efface tous les événements de la base de données.
     * 
     * @return la redirection vers la route de l'agenda
     */
    
    /**
	 * @Security("has_role('ROLE_ADMIN')")
	 */	
    public function toutSupprimerAction() {
		$eventsParent = $this->getDoctrine()->getManager()->getRepository('StapsCalendrierBundle:EventParent')->findAll();
		foreach ($eventsParent as $ep) {
			$this->getDoctrine()->getManager()->remove($ep);
		}
		
		$this->getDoctrine()->getManager()->flush();
		
		return $this->redirectToRoute('staps_calendrier_gererDisponibilites');
	}
	
    /**
     * Importe le fichiers de disponibilités des enseignants et met à jour la base de données.
     * 
     * @return la redirection vers la route de l'agenda
     */
				
	/**
	 * @Security("has_role('ROLE_RESPONSABLE')")
	 */			
    public function dispoAction() {
    	$extensions_valides = array('xml', 'ods', 'xls'); // Array si d'autres sont acceptées
    	$extension_upload = strtolower(substr(strrchr($_FILES['file']['name'], '.'), 1));

		if (in_array($extension_upload,$extensions_valides)) {	
			$destination_path = getcwd()."/transferts/imports/";
		   	$result = 0;
		   	$target_path = $destination_path.basename($_FILES['file']['name']);
		 
		   	if (@move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
			  	$result = 1;
		   	}
		   	
		   	$this->importerDispo($target_path);
		}
		else {
			$this->addFlash(
						'erreur',
						"Le fichier n'est pas du bon type."
					);
		}
	   	return $this->redirectToRoute('staps_calendrier_gererDisponibilites');	   	
    }
    
	/**
     * Met à jour les informations concernant les enseignants et leurs disponibilités dans la base de données
     * 
     * @param {
     *  $file string
     * }
     */
    private function importerDispo($file) {
		$PHPExcel = \PHPExcel_IOFactory::load($file);
		$em = $this->getDoctrine()->getManager();
		
		if (true) { // Condition à trouver
			// On supprime tous les événements d'enseignant
			$em->getRepository('StapsCalendrierBundle:EventParent')->deleteByClassName('enseignant');

			foreach ($PHPExcel->getAllSheets() as $feuille) {			
				$firstRow = true;
				
				foreach ($feuille->getRowIterator() as $ligne) {
					if ($firstRow) {
						$firstRow = false;
						continue;
					}					
					
					$infos = array();					
					foreach ($ligne->getCellIterator() as $cellule) {
						$infos[] = $cellule->getValue();
					}
					
					if (strtolower($infos[4]) != "pas encore répondu") {
						$event = new Event();
						$event->setEventParent(new EventParent());
												
						list($jour, $heure) = explode(' : ', $infos[4]);
						
						$prof = $em->getRepository('StapsUserBundle:Enseignant')->findOneBy(
							array('nom' => $infos[0], 'prenom' => $infos[1]));
						
						if ($prof === null) {
							$prof = new Enseignant();
							$prof->setNom($infos[0]);
							$prof->setPrenom($infos[1]);
							
							$em->persist($prof);
							$em->flush();
						}				
							
						$event->getEventParent()->setEnseignant($prof);
						
						$event->getEventParent()->setTitle($infos[0].' '.$infos[1]);
						$event->getEventParent()->setClassName('enseignant');
						$event->getEventParent()->setDow($this->formaterPeriode($jour));
						$event->getEventParent()->setStart($this->formaterHeure(' à ', $heure));
						$event->getEventParent()->setEnd($this->formaterHeure(' à ', $heure, false));
						$event->getEventParent()->setAllDay(false);
						$event->getEventParent()->setFrequency('hebdomadaire');
						$event->setDateStart($this->formaterDate($_POST['debut']));
						$event->setDateEnd($this->formaterDate($_POST['fin'], false));
						
						$em->persist($event);
					}
				}
			}
			
			$em->flush();
		}
		else {
			$this->addFlash(
						'erreur',
						"Le fichier ne correspond pas au fichier demandé."
					);
		}
	}
}
