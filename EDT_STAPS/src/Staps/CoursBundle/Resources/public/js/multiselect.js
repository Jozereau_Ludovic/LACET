$(function() {	
	function addECById(ue_id) {
		$.ajax({
		   	url: '/cours/recupererEC',
		   	data: 'ue_id=' + ue_id,
		   	type: "POST",
		   	dataType: 'json',
		   	complete: function(data) {
		   		var json = $.parseJSON(data.responseText);

		   		for (var i = 0; i < json.length; ++i) {				
		   			document.getElementById('EC_select').add(
						new Option(json[i].ec_num + " - " + json[i].ec_int, json[i].ec_id)
					);
		   		}
		   	}
		});
	}
	
	function addAPSAById(ec_id) {
		$.ajax({
		   	url: '/cours/recupererAPSA',
		   	data: 'ec_id=' + ec_id,
		   	type: "POST",
		   	dataType: 'json',
		   	complete: function(data) {
		   		var json = $.parseJSON(data.responseText);

		   		for (var i = 0; i < json.length; ++i) {				
		   			document.getElementById('APSA_select').add(
						new Option(json[i].apsa_int, json[i].apsa_id)
					);
		   		}
		   	}
		});
	}
	
	function removeECById(ue_id) {
		$.ajax({
		   	url: '/cours/recupererEC',
		   	data: 'ue_id=' + ue_id,
		   	type: "POST",
		   	dataType: 'json',
		   	complete: function(data) {
		   		var json = $.parseJSON(data.responseText);
		   		
		   		for (var i = 0; i < json.length; ++i) {
					removeAPSAById(json[i].ec_id);
		   			$("#EC_select option[value='" + json[i].ec_id + "']").remove();
		   		}
		   	}
		});
	}
	
	function removeAPSAById(ec_id) {
		$.ajax({
		   	url: '/cours/recupererAPSA',
		   	data: 'ec_id=' + ec_id,
		   	type: "POST",
		   	dataType: 'json',
		   	complete: function(data) {
		   		var json = $.parseJSON(data.responseText);
		   		
		   		for (var i = 0; i < json.length; ++i) {
		   			$("#APSA_select option[value='" + json[i].apsa_id + "']").remove();
		   		}
		   	}
		});
	}
	
	// Thème des multiselect  
	$(".multiselect").select2({
		theme: "bootstrap",
		language: "fr",
		sorter: function(data) {
			return data.sort(function (a, b) {
				if (a.text > b.text) {
					return 1;
				}
				if (a.text < b.text) {
					return -1;
				}
				return 0;
			});
		}
	});
						
	// Lorsqu'on sélectionne une UE, les EC correspondants s'ajoutent dans la liste des EC
	$("#UE_select").on('select2:select', function (e) {
		addECById(e.params.data.id);
	});
		
	// Lorsqu'on sélectionne un EC, les APS correspondantes s'ajoutent dans la liste des APS
	$("#EC_select").on('select2:select', function (e) { // A changer avec la maquette
		addAPSAById(e.params.data.id);
	});
		
	// Lorsqu'on désélectionne une UE, les correspondances s'enlèvent en cascade
	$("#UE_select").on('select2:unselect', function (e) {
		removeECById(e.params.data.id);
	});
		
	// Lorsqu'on désélectionne un EC, on enlève les APS correspondantes
	$("#EC_select").on('select2:unselect', function (e) {		
		removeAPSAById(e.params.data.id);
	});
});
