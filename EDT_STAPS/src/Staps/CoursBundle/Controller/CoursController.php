<?php

namespace Staps\CoursBundle\Controller;

set_include_path(implode(PATH_SEPARATOR, [
    realpath(__DIR__ . '/../../Classes'),
    get_include_path()
]));
require_once 'PHPExcel/IOFactory.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Staps\CoursBundle\Entity\UE;
use Staps\CoursBundle\Entity\EC;
use Staps\CoursBundle\Entity\APSA;
use Staps\CoursBundle\Entity\Lieu;
use Staps\CalendrierBundle\Entity\Event;
use Staps\CalendrierBundle\Entity\EventParent;
use Staps\UserBundle\Entity\Enseignant;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class CoursController extends Controller {
    /**
     * Affiche la page d'importation des données.
     * 
     * @return la vue de la page d'importation des données
     */
     
	/**
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function importerAction() {
		return $this->render('StapsCoursBundle:Cours:maquette.html.twig');
	}
	
    /**
     * Affiche la page de choix des cours avec les liste pré-remplies en fonction de l'utilisateur
     * 
     * @return la vue de la page des choix de cours avec les informations de pré-remplissage
     */
     
    /**
	 * @Security("has_role('ROLE_PROF')")
	 */
    public function gererCoursAction() {
    	$prof = $this->container->get('security.token_storage')->getToken()->getUser()->getEnseignant();
    	$em = $this->getDoctrine()->getManager();

		// On récupère toutes les UE de la maquette
		$ue = $em->getRepository('StapsCoursBundle:UE')->findAll();
			
		// On récupère les EC choisis par l'enseignant
		$selected_ec = $em->getRepository('StapsCoursBundle:EC')->findIdByEnseignant($prof);
		
		// On récupère les APSA choisies par l'enseignant
		$selected_apsa = $em->getRepository('StapsCoursBundle:APSA')->findIdByEnseignant($prof);
			
		// On récupère les UE liés aux choix de l'enseignant
		$linked_ue = $em->getRepository('StapsCoursBundle:UE')->findByEnseignant($prof);
		
		// On récupère les EC liés aux UE précédentes
		$linked_ec = $em->getRepository('StapsCoursBundle:EC')->findByUe($linked_ue);
		
		// On récupère les APSA liés aux EC précédents
		$linked_apsa = $em->getRepository('StapsCoursBundle:APSA')->findByEcs($linked_ec);
		
		$linked_ue_num = array();
		foreach ($linked_ue as $l_ue) {
			$linked_ue_num[] = $l_ue->getNumero();
		}		
		
        return $this->render('StapsCoursBundle:Cours:gererCours.html.twig', 
        	array("UE" => $ue, "selected_EC" => $selected_ec, "selected_APSA" => $selected_apsa, 
				"linked_UE" => $linked_ue, "linked_UE_num" => $linked_ue_num, 
				"linked_EC" => $linked_ec, "linked_APSA" => $linked_apsa, 
			)
		);
    }
	
    /**
     * Importe les différents fichiers de données et met à jour la base de données.
     * 
     * @return la redirection vers la route des importations de fichiers de données
     */
     
    /**
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function uploadAction() {
		$extensions_valides = array('xml', 'ods', 'xls'); // Array si d'autres sont acceptées
		
		// Récupération des différents fichiers
		$files = array(
			'lieux' => $_FILES['lieux'],
			'apsa' => $_FILES['apsa'],
			'apsa_lieux' => $_FILES['apsa_lieux'],
			'maquette' => $_FILES['maquette'],
			'choix' => $_FILES['choix'],
		);

		// On vérifie que les 3 fichiers sont renseignés ou non renseignés et que la maquette est bien là
		if (
			(
				($files['lieux']['name'] == null && $files['lieux']['name'] == null && $files['lieux']['name'] == null)
				|| ($files['lieux']['name'] != null && $files['lieux']['name'] != null && $files['lieux']['name'] != null)
			)
			&& $files['maquette']['name'] != null) {
				
			foreach ($files as $key => $file) {				
				if ($file['name'] != null) {
					$extension_upload = strtolower(substr(strrchr($file['name'], '.'), 1));
					if (in_array($extension_upload, $extensions_valides)) {	
						$destination_path = getcwd()."/transferts/imports/";
						$result = 0;
						$target_path = $destination_path.basename($file['name']);
					 
						if (@move_uploaded_file($file['tmp_name'], $target_path)) {
							$result = 1;
						}
						
						switch ($key) {
							case 'lieux' : $this->importerLieux($target_path);
							break;
							case 'apsa' : $this->importerAPSA($target_path);
							break;
							case 'apsa_lieux' : $this->importerCorrespondance($target_path);
							break;
							case 'maquette' : $this->importerMaquette($target_path);
							break;
							case 'choix' : $this->reimporterChoix($target_path);
							break;
						}
					}
					else {
						switch ($key) {
							case 'lieux' : $tip = "des installations";
							break;
							case 'apsa' : $tip = "des APSA";
							break;
							case 'apsa_lieux' : $tip = "des correspondances APSA-installations";
							break;
							case 'maquette' : $tip = "de la maquette";
							break;
							case 'choix' : $tip = "des choix";
							break;
						}
						
						$this->addFlash(
							'erreur',
							"Le fichier $tip n'est pas du bon type."
						);
					} 
				}
			}
		}
		else {
			if ($files['maquette']['name'] == null) {
				$this->addFlash(
					'erreur',
					"La maquette est obligatoire."
				);
			}
			if  (!(($files['lieux']['name'] == null && $files['lieux']['name'] == null && $files['lieux']['name'] == null)
				|| ($files['lieux']['name'] != null && $files['lieux']['name'] != null && $files['lieux']['name'] != null))) {
				$this->addFlash(
					'erreur',
					"Les fichiers de réservation des installations, des APSA et
					des correspondances doivent être fournis en même temps."
				);
			}
		}
	   	return $this->redirectToRoute('staps_cours_importer');	   
	}
	
    /**
     * Récupère les EC à modifier dans leur liste depuis l'UE modifiée dans sa liste.
     * 
     * @param {
     *  $request Request
     * }
     * 
     * @return Response
     */
     
    /**
	 * @Security("has_role('ROLE_PROF')")
	 */
	public function recupererECAction(Request $request) {
		if ($request->isXmlHttpRequest()) {
			$em = $this->getDoctrine()->getManager();
			$ue_id = $_POST['ue_id'];
			
			// On récupère les EC liés à l'UE
			$ec = $em->getRepository('StapsCoursBundle:EC')->findIdIntituleNumeroByUEId($ue_id);

			$response = new Response(json_encode($ec));
			$response->headers->set('Content-Type', 'application/json; charset=UTF-8');
			
			return $response;
		}
		
		return new Response(null);	
	}
	
    /**
     * Récupère les APSA à modifier dans leur liste depuis l'EC modifié dans sa liste.
     * 
     * @param {
     *  $request Request
     * }
     * 
     * @return Response
     */
     
    /**
	 * @Security("has_role('ROLE_PROF')")
	 */
	public function recupererAPSAAction(Request $request) {
		if ($request->isXmlHttpRequest()) {
			$em = $this->getDoctrine()->getManager();
			$ec_id = $_POST['ec_id'];
			
			// On récupère les EC liés à l'UE
			$apsa = $em->getRepository('StapsCoursBundle:APSA')->findIdIntituleByECId($ec_id);
			
			$response = new Response(json_encode($apsa));
			$response->headers->set('Content-Type', 'application/json; charset=UTF-8');
			
			return $response;
		}
		
		return new Response(null);	
	}
	
	/**
     * Sauvegarde en base de données les choix de cours de l'utilisateur.
     * 
     * @param {
     *  $request Request
     * }
     * 
     * @return la redirectionn vers la route du choix des cours
     */
     
	/**
	 * @Security("has_role('ROLE_PROF')")
	 */
	public function sauvegarderCoursAction(Request $request) {
		if ($request->isXmlHttpRequest()) {
			$prof = $this->container->get('security.token_storage')->getToken()->getUser()->getEnseignant();
			
			foreach ($prof->getECs() as $ec) {
				$prof->removeEC($ec);
			}
			
			foreach ($prof->getAPSAs() as $apsa) {
				$prof->removeAPSA($apsa);
			}
			
			$em = $this->getDoctrine()->getManager();
			$ec_id = $_POST['ec_id'];
			$apsa_id = $_POST['apsa_id'];
			
			if ($ec_id != null) {
				foreach (explode(',', $ec_id) as $id) {
					$ec = $em->getRepository('StapsCoursBundle:EC')->find($id);
					$prof->addEC($ec);
				}
			}
			
			if ($apsa_id != null) {
				foreach (explode(',', $apsa_id) as $id) {
					$apsa = $em->getRepository('StapsCoursBundle:APSA')->find($id);
					$prof->addAPSA($apsa);
				}
			}
			
			$em->flush();
		}
		
		return $this->redirectToRoute('staps_cours_gererCours');
	}
	
	/**
     * Télécharge le fichier des choix des cours depuis la base de données.
     * 
     * @return Response
     */
     
	/**
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function telechargerChoixAction() {		
		$em = $this->getDoctrine()->getManager();
		
		// Création des objets de base et initialisation des informations d'entête
		$classeur = new \PHPExcel;
		$classeur->getProperties()->setCreator("Export");
		$classeur->setActiveSheetIndex(0);
		$feuille = $classeur->getActiveSheet();

		// Ajout des données dans la feuille de calcul
		$feuille->setTitle('Choix des UE par enseignant');
		$feuille->setCellValue('A1', 'Numéro d\'EC');
		$feuille->setCellValue('B1', 'Intitulé d\'EC');
		$feuille->setCellValue('C1', 'Nom enseignant');
		$feuille->setCellValue('D1', 'Prénom enseignant');

		// On récupère les EC et enseignants liés
		$results = $em->getRepository('StapsCoursBundle:EC')->findWithEnseignants();
		
		$ligne = 2;

		foreach ($results as $r) {
			$feuille->setCellValueByColumnAndRow(0, $ligne, $r['ec_num']);
			$feuille->setCellValueByColumnAndRow(1, $ligne, $r['ec_int']);
			$feuille->setCellValueByColumnAndRow(2, $ligne, $r['p_nom']);
			$feuille->setCellValueByColumnAndRow(3, $ligne++, $r['p_prenom']);
		}
		
		$classeur->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$classeur->getActiveSheet()->getColumnDimension('B')->setWidth(50);
		$classeur->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$classeur->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
 
		$writer = \PHPExcel_IOFactory::createWriter($classeur, 'Excel2007'); 
		$writer->save('transferts/exports/export_choix.xls');
		
		// On créé la réponse pour télécharger le fichier d'export
   		$response = new Response();
   		$response->setContent(file_get_contents('transferts/exports/export_choix.xls'));
   		$response->headers->set('Content-Type', 'application/force-download');
   		$response->headers->set('Content-disposition', 'filename=choix.xls');
         
   		return $response;
	}
	
    /**
     * Met à jour les informations concernant la maquette dans la base de données.
     * 
     * @param {
     *  $maquetteFile string
     * }
     */
	private function importerMaquette($maquetteFile) {
		$em = $this->getDoctrine()->getManager();
		
		// On supprime toute la maquette				
		$profs = $em->getRepository('StapsUserBundle:Enseignant')->findAll();
		foreach ($profs as $prof) {
			foreach ($prof->getECs() as $ec) {
				$prof->removeEC($ec);
			}
			
			foreach ($prof->getAPSAs() as $apsa) {
				$prof->removeAPSA($apsa);
			}
		}
		
		$ue = $em->getRepository('StapsCoursBundle:UE')->findAll();
		foreach ($ue as $u) {
			$em->remove($u);
		}
		
		$em->flush();
		
		// On charge le fichier de la nouvelle maquette
		$PHPExcel = \PHPExcel_IOFactory::load($maquetteFile);
		
		foreach ($PHPExcel->getAllSheets() as $feuille) {
			// On récupère les colonnes concernant les informations sur les UE
			$colNumUE = $this->getColumnByValue($feuille, 'n°UE');
			$colIntUE = $this->getColumnByValue($feuille, 'Intitulé UE');
			
			// Puis celles sur les EC
			$colIntEC = $this->getColumnByValue($feuille, 'Intitulés EC');
			$colCodeEC = $this->getColumnByValue($feuille, 'Intitulés EC courts');
			$colEffEC = $this->getColumnByValue($feuille, 'Effectifs prévisionnels');
			$colParcEC = $this->getColumnByValue($feuille, 'n° parcours (rien si tronc commun)');
			$colCM_UE = $this->getColumnByValue($feuille, 'CM');
			$colTD_UE = $this->getColumnByValue($feuille, 'TD');
			$colTP_UE = $this->getColumnByValue($feuille, 'TP');
			$colPartiEC = $this->getColumnByValue($feuille, 'Particularités');
			
			// Et sur les APSA
			$colAPSA = $this->getColumnByValue($feuille, 'APSA');
			
			// On vérifie que la feuille possède toutes les informations nécessaires
			if (!(in_array(null, array($colNumUE, $colIntUE, $colCodeEC, $colIntEC, $colEffEC, $colParcEC, 
				$colParcEC, $colCM_UE, $colTD_UE, $colTP_UE, $colPartiEC, $colAPSA)))) {
				for ($ligne = 8; $ligne <= $feuille->getHighestRow(); ++$ligne) {
					
					// On vérifie les informations de l'UE
					if ($feuille->getCell($colNumUE.$ligne)->getValue() != null 
						&& $feuille->getCell($colIntUE.$ligne)->getValue() != null) {
						$nbEC = 0;
						$ue = new UE();
						
						$ue->setIntitule($feuille->getCell($colIntUE.$ligne)->getValue());
						$ue->setNumero($feuille->getCell($colNumUE.$ligne)->getValue());
						
						$em->persist($ue);
					}
				
					if ($feuille->getCell($colEffEC.$ligne)->getValue() != null) {
						$ec = new EC();
						
						$intEC = $feuille->getCell($colIntEC.$ligne)->getValue();
						if ($intEC != null) {
							$ec->setIntitule($intEC);
						}
						else {
							$ec->setIntitule($ue->getIntitule());
						}
						
						$codeEC = $feuille->getCell($colCodeEC.$ligne)->getValue();
						if ($codeEC != null) {
							$ec->setCode($codeEC);
						}
						else {
							$ec->setCode($ec->getIntitule());
						}

						$ec->setNumero($ue->getNumero().$nbEC++);
						$ec->setEffectif($feuille->getCell($colEffEC.$ligne)->getValue());
						
						$ec->setParcours($feuille->getCell($colParcEC.$ligne)->getValue());
						if ($ec->getParcours() == null) {
							$ec->setParcours('1,2,3,4');
						}		
						else {
							$ec->setParcours(str_replace('.', ',', $ec->getParcours()));
						}
						
						$particularites = $feuille->getCell($colPartiEC.$ligne)->getValue();
						if (strpos($particularites, "impaire") !== false) {
							$ec->setPaire(false);
						}
						else if (strpos($particularites, "paire") !== false) {
							$ec->setPaire(true);
						}
						else {
							$ec->setPaire(null);
						}

						if ($ec->getPaire() != null) {
							$ec->setCoursPrecedent($particularites);
						}
						
						foreach (array("CM", "TP", "TD") as $type) {
							$ecType = $this->creerType($feuille, $ligne, $type, clone $ec);
							
							if ($ecType != null) {
								if ($feuille->getCell($colAPSA.$ligne)->getValue() != null) {				
									foreach (explode(', ', $feuille->getCell($colAPSA.$ligne)->getValue()) as $code_apsa) {
										$apsa = $em->getRepository('StapsCoursBundle:APSA')->findByPartCode($code_apsa);
										
										foreach ($apsa as $a) {
											$ecType->addAPSA($a);
										}
									}
								}
								
								$ue->addEC($ecType);
					
								$em->persist($ecType);
							}
						}							
					}
				}
			}	
		}
		
		$em->flush();	
	}
	
	/**
     * Met à jour les informations concernant les APSA dans la base de données.
     * 
     * @param {
     *  $file string
     * }
     */
	private function importerAPSA($file) {
		$em = $this->getDoctrine()->getManager();
		
		// On supprime toutes les APSA				
		$results = $em->getRepository('StapsCoursBundle:APSA')->findAll();
		foreach ($results as $r) {
			$em->remove($r);
		}
		
		$em->flush();
		
		// On charge le fichier de la nouvelle maquette
		$PHPExcel = \PHPExcel_IOFactory::load($file);
		
		$feuille = $PHPExcel->getActiveSheet();
		
		// On récupère les colonnes concernant les informations utiles sur les APSA
		$colNom_APSA = $this->getColumnByValue($feuille, 'APSA');
		$colCode = $this->getColumnByValue($feuille, 'Intitulé abrégé');
			
		// On vérifie que la feuille possède toutes les informations nécessaires
		if ($colNom_APSA != null && $colCode != null) {
			for ($ligne = 2; $ligne <= $feuille->getHighestRow(); ++$ligne) {
					
				// On vérifie les informations de l'UE
				if ($feuille->getCell($colNom_APSA.$ligne)->getValue() != null 
					&& $feuille->getCell($colCode.$ligne)->getValue() != null) {						
					$apsa = new APSA();
							
					$apsa->setIntitule($feuille->getCell($colNom_APSA.$ligne)->getValue());
					$apsa->setCode($feuille->getCell($colCode.$ligne)->getCalculatedValue());
							
					$em->persist($apsa);
				}
			}
		}	
		else {
			$this->addFlash(
					'erreur',
					"Colonne 'APSA' ou 'Intitulé abrégé' non trouvée(s)."
			);
		}	
		$em->flush();
		$em->getRepository('StapsCoursBundle:APSA')->deleteDouble();		
	}
	
	/**
     * Met à jour les informations concernant les correspondances APSA-installations dans la base de données.
     * 
     * @param {
     *  $file string
     * }
     */
	private function importerCorrespondance($file) {
		$em = $this->getDoctrine()->getManager();
		
		// On charge le fichier de la nouvelle maquette
		$PHPExcel = \PHPExcel_IOFactory::load($file);
		
		foreach ($PHPExcel->getAllSheets() as $feuille) {
			foreach ($feuille->getRowIterator() as $line => $ligne) {				
				foreach ($ligne->getCellIterator() as $row => $cellule) {
					if ($cellule->getValue() == 'X') {
						$code_lieu = $feuille->getCell($row.'1')->getValue();
						$code_apsa = $feuille->getCell('A'.$line)->getValue();
						
						$lieu = $em->getRepository('StapsCoursBundle:Lieu')->findOneByCode($code_lieu);		
								
						$apsa = $em->getRepository('StapsCoursBundle:APSA')->findByPartCode($code_apsa);
						
						foreach ($apsa as $a) {
							$a->addLieux($lieu);
							
							$em->persist($a);
						}

						$em->flush();
					}
				}
			}
		}	
	}
	
	/**
     * Met à jour les informations concernant les lieux dans la base de données.
     * 
     * @param {
     *  $file string
     * }
     */
    private function importerLieux($file) {		
		$em = $this->getDoctrine()->getManager();
		$PHPExcel = \PHPExcel_IOFactory::load($file);
		
		if (strtolower($PHPExcel->getSheet()->getCell("A1")->getValue()) == "localité") {
			//On supprime tous les événements de lieu
			$em->getRepository('StapsCalendrierBundle:EventParent')->deleteByClassName('lieu');
			$lieux = $em->getRepository('StapsCoursBundle:Lieu')->findAll();
			foreach ($lieux as $l) {
				$em->remove($l);
			}
		
			$em->flush();

			foreach ($PHPExcel->getAllSheets() as $feuille) {
				// On récupère les colonnes concernant les informations sur les lieux
				$colLocalite = $this->getColumnByValue($feuille, 'Localité');
				$colInstall = $this->getColumnByValue($feuille, 'Installation');
				$colZone = $this->getColumnByValue($feuille, 'activité');
				$colCode = $this->getColumnByValue($feuille, 'Code final');
				$colCapacite = $this->getColumnByValue($feuille, 'Capacité');
				$colJour = $this->getColumnByValue($feuille, 'Jour souhaité');
				$colHoraires = $this->getColumnByValue($feuille, 'Horaires');
				$colDebut = $this->getColumnByValue($feuille, 'Début période');
				$colFin = $this->getColumnByValue($feuille, 'Fin période');	

				// On vérifie que la feuille possède toutes les informations nécessaires
				if (!(in_array(null, array($colLocalite, $colInstall, $colZone, $colCode, $colCapacite, 
					$colJour, $colHoraires, $colDebut, $colFin)))) {
						
					for ($ligne = 2; $ligne <= $feuille->getHighestRow(); ++$ligne) {					
						if (($lieu = $em->getRepository('StapsCoursBundle:Lieu')
							->findOneByCode($feuille->getCell($colCode.$ligne)->getCalculatedValue())) == null) {
							$lieu = new Lieu();
							
							$lieu->setLocalite($feuille->getCell($colLocalite.$ligne)->getValue());
							$lieu->setInstallation($feuille->getCell($colInstall.$ligne)->getValue());
							$lieu->setZoneActivite($feuille->getCell($colZone.$ligne)->getValue());
							$lieu->setCode($feuille->getCell($colCode.$ligne)->getCalculatedValue());
							$lieu->setCapacite($feuille->getCell($colCapacite.$ligne)->getCalculatedValue());
							
							if (!in_array(null, array($lieu->getLocalite(), 
								$lieu->getInstallation(), $lieu->getZoneActivite(), $lieu->getCode()))) {
								$em->persist($lieu);
							}
							else {
								continue;
							}
						}

						$event = new Event();
						$event->setEventParent(new EventParent());
						
						$debut = date('Y-m-d', \PHPExcel_Shared_Date::ExcelToPHP($feuille->getCell($colDebut.$ligne)->getValue()));
						$fin = date('Y-m-d', \PHPExcel_Shared_Date::ExcelToPHP($feuille->getCell($colFin.$ligne)->getValue()));
						
						$jour = $feuille->getCell($colJour.$ligne)->getValue();
						$horaires = $feuille->getCell($colHoraires.$ligne)->getValue();
						
						$event->getEventParent()->setTitle('Localité : '.$lieu->getLocalite().
						'\nInstallation : '.$lieu->getInstallation().'\nZone d\'activité : '.$lieu->getZoneActivite());
						$event->getEventParent()->setClassName("lieu");
						$event->getEventParent()->setLieu($lieu);
						$event->getEventParent()->setDow($this->formaterPeriode($jour));
						$event->getEventParent()->setStart($this->formaterHeure(" à ", $horaires));
						$event->getEventParent()->setEnd($this->formaterHeure(" à ", $horaires, false));
						$event->getEventParent()->setAllDay(false);
						$event->getEventParent()->setFrequency('hebdomadaire');
						$event->setDateStart($this->formaterDate($debut));
						$event->setDateEnd($this->formaterDate($fin, false));
						
						$em->persist($event);
						$em->flush();
					}
				}
			}
		}
		else {
			$this->addFlash(
						'erreur',
						"Le fichier ne correspond pas au fichier demandé."
					);
		}
	}
	

    /**
     * Réaffecte les choix de cours des enseignants dans la base de données.
     * 
     * @param {
     *  $file string
     * }
     */
     
	/**
	 * @Security("has_role('ROLE_ADMIN')")
	 */
	public function reimporterChoix($file) {
		$PHPExcel = \PHPExcel_IOFactory::load($file);
		
		if (strtolower($PHPExcel->getSheet()->getCell("A1")->getValue()) == "numéro d'ec") {
			foreach ($PHPExcel->getAllSheets() as $feuille) {
				$firstRow = true;
				
				foreach ($feuille->getRowIterator() as $ligne) {
					if ($firstRow) {
						$firstRow = false;
						continue;
					}
					
					$infos = array();
					
					foreach ($ligne->getCellIterator() as $cellule) {
						$infos[] = $cellule->getValue();
					}
					
					$prof = $this->getDoctrine()->getManager()->getRepository('StapsUserBundle:Enseignant')
						->findOneBy(array('nom' => $infos[2], 'prenom' => $infos[3]));
					$ec = $this->getDoctrine()->getManager()->getRepository('StapsCoursBundle:EC')
						->findOneBy(array('numero' => $infos[0], 'intitule' => $infos[1]));
					
					if ($ec != null) {
						$prof->addCours($ec);
					}
				}
			}
			
			$this->getDoctrine()->getManager()->flush();
		}
		else {
			$this->addFlash(
						'erreur',
						"Le fichier ne correspond pas au fichier demandé."
					);
		}
	}
	
	/**
     * Créé un EC de type CM, TD ou TP si les informations existantes en maquette le permettent.
     * 
     * @param {
     *  $feuille feuille
     *  $ligne integer
     *  $type string
     *  $ec Staps\CoursBundle\Entity\EC
     * }
     * 
     * @return Staps\CoursBundle\Entity\EC ou null
     */
	private function creerType($feuille, $ligne, $type, $ec) {
		if (($contenu = $feuille->getCell($this->getColumnByValue($feuille, $type).$ligne)->getValue()) != null) {
			
			$ec->setHeures($contenu);
			$ec->setTypeCours($type);
			$ec->setIntitule($ec->getIntitule()." (".$type.")");
			
			return $ec;
		}
		
		return null;
	}
    
    /**
     * Récupère la colonne de la cellule contenant $value.
     * 
     * @param {
     *  $feuille feuille
     *  $value string
     * }
     * 
     * @return column ou null
     */
    private function getColumnByValue($feuille, $value) {
    	foreach ($feuille->getRowIterator() as $ligne) {
    		foreach ($ligne->getCellIterator() as $cellule) {	
				if (stripos($cellule->getCalculatedValue(), $value) > -1) {
					return $cellule->getColumn();
				}
			}
		}
		
		return null;
	}
	
	/**
     * Formate les jours données en tableau d'entiers.
     * 
     * @param {
     *  $jours string
     * }
     * 
     * @return array ou null
     */
	private function formaterPeriode($jours) {
		if ($jours != null) {
			$jours = strtolower($jours);
			
			$dow = '[';
			foreach (explode(", ", $jours) as $jour) {
				switch ($jour) {
					case "lundi": $dow = $dow."1,";
					break;
					case "mardi": $dow = $dow."2,";
					break;
					case "mercredi": $dow = $dow."3,";
					break;
					case "jeudi": $dow = $dow."4,";
					break;
					case "vendredi": $dow = $dow."5,";
					break;
					case "samedi": $dow = $dow."6,";
					break;
				}
			}
			$dow = $dow."]";
			$dow = str_replace(",]", "]", $dow);
			
			return $dow;
		}
		return null;
	}
	
	/**
     * Formate l'heure en HH:MM:00.
     * 
     * @param {
     *  $delemiteur string
     *  $heure string
     *  $debut boolean
     * }
     * 
     * @return \Datetime ou null
     */
	private function formaterHeure($delimiteur, $heure, $debut = true) {
		if ($heure != null) {
			($debut) ? $i = 0 : $i = 1;			
			$HM = explode($delimiteur, $heure)[$i];
			
			list($H, $M) = explode("h", strtolower($HM));
			if ($M == "") $M = "00";
			
			return new \Datetime($H.':'.$M.':00');
		}
		return null;
	}
	
	/**
     * Formate la date et ajoute un jour si c'est la date de fin de période.
     * 
     * @param {
     *  $date string
     *  $debut boolean
     * }
     * 
     * @return \Datetime ou null
     */
	private function formaterDate($date, $debut = true) {
		if ($date != null) {
			$dateFormate = new \Datetime($date);
			
			if (!$debut) {
				$dateFormate->modify("+1 day");
			}
			
			return $dateFormate;
		}
		return null;
	}
	
	/**
     * Affecte des choix de cours aux enseignants dans la base de données.
     * 
     * @param {
     *  $file string
     * }
     */
     
	/**
	 * @Security("has_role('ROLE_RESPONSABLE')")
	 */
	public function importerChoixAction() {
		$extensions_valides = array('xml', 'ods', 'xls'); // Array si d'autres sont acceptées
    	$extension_upload = strtolower(substr(strrchr($_FILES['file']['name'], '.'), 1));

		if (in_array($extension_upload,$extensions_valides)) {	
			$destination_path = getcwd()."/transferts/imports/";
		   	$result = 0;
		   	$target_path = $destination_path.basename($_FILES['file']['name']);
		 
		   	if (@move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
			  	$result = 1;
		   	}
		   	
		   	$this->importerChoix($target_path);
		}
		else {
			$this->addFlash(
						'erreur',
						"Le fichier n'est pas du bon type."
					);
		}
	   	return $this->redirectToRoute('staps_cours_gererCours');	   	
	}
	
	/**
     * Importe le fichiers des choix de cours des enseignants et met à jour la base dde données.
     * 
     * @param {
     *  $file string
     * }
     */
	private function importerChoix($file) {
		$em = $this->getDoctrine()->getManager();
		
		// On charge le fichier de la nouvelle maquette
		$PHPExcel = \PHPExcel_IOFactory::load($file);
		
		foreach ($PHPExcel->getAllSheets() as $feuille) {
			foreach ($feuille->getRowIterator() as $line => $ligne) {				
				foreach ($ligne->getCellIterator() as $row => $cellule) {
					if ($cellule->getValue() == 'X') {
						$nom = $feuille->getCell('A'.$line)->getValue();
						$prenom = $feuille->getCell('B'.$line)->getValue();
						$code_apsa = $feuille->getCell($row.'1')->getValue();
						
						$prof = $em->getRepository('StapsUserBundle:Enseignant')
							->findOneBy(array('nom' => $nom, 'prenom' => $prenom));		
							
						if ($prof === null) {
							$prof = new Enseignant();
							$prof->setNom($nom);
							$prof->setPrenom($prenom);
							
							$em->persist($prof);
							$em->flush();
						}
								
						$apsa = $em->getRepository('StapsCoursBundle:APSA')->findByPartCode($code_apsa);
						
						foreach ($apsa as $a) {
							$prof->addAPSA($a);
							
							$em->persist($prof);
						}

						$em->flush();
					}
				}
			}
		}	
	}
}
