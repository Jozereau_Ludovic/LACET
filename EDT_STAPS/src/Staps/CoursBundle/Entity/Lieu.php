<?php

namespace Staps\CoursBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lieu
 *
 * @ORM\Table(name="lieu")
 * @ORM\Entity(repositoryClass="Staps\CoursBundle\Repository\LieuRepository")
 */
class Lieu {
	/**
     * Constructor
     */
    public function __construct() {
        $this->apsas = new \Doctrine\Common\Collections\ArrayCollection();
    }
	
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="localite", type="string", length=255)
     */
    private $localite;
    
    /**
     * @var string
     *
     * @ORM\Column(name="installation", type="string", length=255)
     */
    private $installation;
    
    /**
     * @var string
     *
     * @ORM\Column(name="zoneActivite", type="string", length=255)
     */
    private $zoneActivite;
    
    /**
   	 * @ORM\ManyToMany(targetEntity="Staps\CoursBundle\Entity\APSA", inversedBy="lieux", cascade={"persist"})
   	 * @ORM\JoinColumn(nullable=true)
   	 */
   	private $apsas;
   	
   	/**
   	 * @var int
   	 * 
   	 * @ORM\Column(name="capacite", type="integer")
   	 */
   	private $capacite;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set localite
     *
     * @param string $localite
     *
     * @return Lieu
     */
    public function setLocalite($localite)
    {
        $this->localite = $localite;

        return $this;
    }

    /**
     * Get localite
     *
     * @return string
     */
    public function getLocalite()
    {
        return $this->localite;
    }

    /**
     * Set installation
     *
     * @param string $installation
     *
     * @return Lieu
     */
    public function setInstallation($installation)
    {
        $this->installation = $installation;

        return $this;
    }

    /**
     * Get installation
     *
     * @return string
     */
    public function getInstallation()
    {
        return $this->installation;
    }

    /**
     * Set zoneActivite
     *
     * @param string $zoneActivite
     *
     * @return Lieu
     */
    public function setZoneActivite($zoneActivite)
    {
        $this->zoneActivite = $zoneActivite;

        return $this;
    }

    /**
     * Get zoneActivite
     *
     * @return string
     */
    public function getZoneActivite()
    {
        return $this->zoneActivite;
    }

    /**
     * Add apsa
     *
     * @param \Staps\CoursBundle\Entity\APSA $apsa
     *
     * @return Lieu
     */
    public function addAPSA(\Staps\CoursBundle\Entity\APSA $apsa)
    {
        $this->apsas[] = $apsa;

        return $this;
    }

    /**
     * Remove apsa
     *
     * @param \Staps\CoursBundle\Entity\APSA $apsa
     */
    public function removeAPSA(\Staps\CoursBundle\Entity\APSA $apsa)
    {
        $this->apsas->removeElement($apsa);
    }

    /**
     * Get apsas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAPSAs()
    {
        return $this->apsas;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Lieu
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set capacite
     *
     * @param integer $capacite
     *
     * @return Lieu
     */
    public function setCapacite($capacite)
    {
        $this->capacite = $capacite;

        return $this;
    }

    /**
     * Get capacite
     *
     * @return integer
     */
    public function getCapacite()
    {
        return $this->capacite;
    }
}
