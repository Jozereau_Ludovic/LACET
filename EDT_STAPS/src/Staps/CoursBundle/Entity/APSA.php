<?php

namespace Staps\CoursBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * APSA
 *
 * @ORM\Table(name="apsa")
 * @ORM\Entity(repositoryClass="Staps\CoursBundle\Repository\APSARepository")
 */
class APSA {
	/**
     * Constructor
     */
    public function __construct()
    {
        $this->ecs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->profs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->lieux = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
	/**
   	 * @ORM\ManyToMany(targetEntity="Staps\CoursBundle\Entity\EC", inversedBy="apsas", cascade={"persist"})
   	 * @ORM\JoinColumn(name="ecs", nullable=true)
   	 */
   	private $ecs;
   	
	/**
   	 * @ORM\ManyToMany(targetEntity="Staps\UserBundle\Entity\Enseignant", inversedBy="apsas", cascade={"persist"})
   	 * @ORM\JoinColumn(name="enseignants", nullable=true)
   	 */
   	private $enseignants;
	
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule", type="string", length=255, nullable=true)
     */
    private $intitule;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;
    
	/**
   	 * @ORM\ManyToMany(targetEntity="Staps\CoursBundle\Entity\Lieu", mappedBy="apsas", cascade={"persist"})
   	 * @ORM\JoinColumn(nullable=true)
   	 */
   	private $lieux;
   		
   	/**
	 * @var int
	 */
	private $heuresPlacees;
	
   	/**
	 * @var int
	 */
	private $groupes;
	
   	/**
	 * @var int
	 */
	private $groupesPlaces;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set intitule
     *
     * @param string $intitule
     *
     * @return APSA
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * Get intitule
     *
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return APSA
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add ec
     *
     * @param \Staps\CoursBundle\Entity\EC $ec
     *
     * @return APSA
     */
    public function addEC(\Staps\CoursBundle\Entity\EC $ec)
    {
        $this->ecs[] = $ec;

        return $this;
    }

    /**
     * Remove ec
     *
     * @param \Staps\CoursBundle\Entity\EC $ec
     */
    public function removeEC(\Staps\CoursBundle\Entity\EC $ec)
    {
        $this->ecs->removeElement($ec);
    }

    /**
     * Get ecs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getECs()
    {
        return $this->ecs;
    }

    /**
     * Add enseignant
     *
     * @param \Staps\UserBundle\Entity\Enseignant $enseignant
     *
     * @return APSA
     */
    public function addEnseignant(\Staps\UserBundle\Entity\Enseignant $enseignant)
    {
        $this->enseignants[] = $enseignant;

        return $this;
    }

    /**
     * Remove enseignant
     *
     * @param \Staps\UserBundle\Entity\Enseignant $enseignant
     */
    public function removeEnseignant(\Staps\UserBundle\Entity\Enseignant $enseignant)
    {
        $this->enseignants->removeElement($enseignant);
    }

    /**
     * Get enseignants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnseignants()
    {
        return $this->enseignants;
    }

    /**
     * Add lieux
     *
     * @param \Staps\CoursBundle\Entity\Lieu $lieux
     *
     * @return APSA
     */
    public function addLieux(\Staps\CoursBundle\Entity\Lieu $lieux)
    {
        $this->lieux[] = $lieux;
        $lieux->addAPSA($this);

        return $this;
    }

    /**
     * Remove lieux
     *
     * @param \Staps\CoursBundle\Entity\Lieu $lieux
     */
    public function removeLieux(\Staps\CoursBundle\Entity\Lieu $lieux)
    {
        $this->lieux->removeElement($lieux);
        $lieux->removeAPSA($this);
    }

    /**
     * Get lieux
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLieux()
    {
        return $this->lieux;
    }
    
        public function getNbGroupes($parametres, $ec) {
		$capaciteCours = $parametres->getCapacite($ec->getTypeCours());
		
		$licence = ceil(substr($ec->getNumero(), 0, 1)/2);
		$effectifPrevu = round($parametres->getEffectifPrevu($licence)/count($ec->getAPSAs()));

		$this->groupes = round(min($ec->getEffectif(), $effectifPrevu)/$capaciteCours);

		return $this->groupes;
	}
	
	public function addHeuresPlacees($ajout) {
		$this->heuresPlacees += $ajout;
	}
	
	public function estFixe($heuresEC) {
		return $this->heuresPlacees >= $heuresEC * $this->groupes;
	}
	
	public function getGroupesPlaces() {
		return $this->groupesPlaces;
	}
	
	public function getGroupes() {
		return $this->groupes;
	}
	
	public function setGroupesPlaces($groupesPlaces) {
		$this->groupesPlaces = $groupesPlaces;
		
		return $this;
	}
	
	public function getHeuresPlacees() {
		return $this->heuresPlacees;
	}
}
