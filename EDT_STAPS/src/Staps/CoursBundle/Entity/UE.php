<?php

namespace Staps\CoursBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UE
 *
 * @ORM\Table(name="ue")
 * @ORM\Entity(repositoryClass="Staps\CoursBundle\Repository\UERepository")
 */
class UE {
	/**
     * Constructor
     */
    public function __construct()
    {
        $this->ecs = new \Doctrine\Common\Collections\ArrayCollection();
    }
	
	/**
   	 * @ORM\OneToMany(targetEntity="Staps\CoursBundle\Entity\EC", mappedBy="ue", cascade={"persist", "remove"})
   	 * @ORM\JoinColumn(nullable=true)
   	 */
  	private $ecs;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule", type="string", length=255)
     */
    private $intitule;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="integer", unique=true)
     */
    private $numero;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set intitule
     *
     * @param string $intitule
     *
     * @return UE
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * Get intitule
     *
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return UE
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Add ec
     *
     * @param \Staps\CoursBundle\Entity\EC $ec
     *
     * @return UE
     */
    public function addEC(\Staps\CoursBundle\Entity\EC $ec)
    {
        $this->ecs[] = $ec;
		$ec->setUE($this);

        return $this;
    }

    /**
     * Remove ec
     *
     * @param \Staps\CoursBundle\Entity\EC $ec
     */
    public function removeEC(\Staps\CoursBundle\Entity\EC $ec)
    {
        $this->ecs->removeElement($ec);
        
        //$ec->setUE(null); car relation non facultative
    }

    /**
     * Get ecs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getECs()
    {
        return $this->ecs;
    }
}
