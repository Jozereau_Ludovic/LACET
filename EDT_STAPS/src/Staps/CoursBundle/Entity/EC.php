<?php

namespace Staps\CoursBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EC
 *
 * @ORM\Table(name="ec")
 * @ORM\Entity(repositoryClass="Staps\CoursBundle\Repository\ECRepository")
 */
class EC {
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->apsas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->profs = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
	/**
   	 * @ORM\ManyToMany(targetEntity="Staps\CoursBundle\Entity\APSA", mappedBy="ecs", cascade={"persist", "remove"})
   	 * @ORM\JoinColumn(name="apsas", nullable=true)
   	 */
  	private $apsas;
	
	/**
   	 * @ORM\ManyToMany(targetEntity="Staps\UserBundle\Entity\Enseignant", inversedBy="ecs", cascade={"persist"})
   	 * @ORM\JoinColumn(name="enseignants", nullable=true)
   	 */
	private $enseignants;
	
	/**
   	 * @ORM\ManyToOne(targetEntity="Staps\CoursBundle\Entity\UE", inversedBy="ecs", cascade={"persist"})
   	 * @ORM\JoinColumn(name="ue", nullable=false)
   	 */
   	private $ue;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule", type="string", length=255, nullable=true)
     */
    private $intitule;
    
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="integer", nullable=true)
     */
    private $numero;

	/**
     * @var int
     *
     * @ORM\Column(name="effectif", type="integer")
     */
    private $effectif;

    /**
     * @var string
     *
     * @ORM\Column(name="parcours", type="string", length=255, nullable=true)
     */
    private $parcours;

    /**
     * @var int
     *
     * @ORM\Column(name="heure", type="integer", nullable=false)
     */
    private $heures;

    /**
     * @var string
     *
     * @ORM\Column(name="typeCours", type="string", length=255, nullable=false)
     */
    private $typeCours;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="paire", type="boolean", nullable=true)
     */
    private $paire;
    
    /**
     * @var string
     *
     * @ORM\Column(name="coursPrecedent", type="string", length=255, nullable=true)
     */
    private $coursPrecedent;
    
	/* Carte des places pour une semaine
	 * Matrice taille = (20-7-1)/dureeCreneau x 6
	 * 
	 * Contient tableau des id des salles et des lieux
	 */
	private $cartePlaces;
	
	/**
	 * @var int 
	 */
	private $heuresPlacees;
	
	/**
	 * @var int
	 */
	private $effectifGroupe;
	
	/**
	 * @var int
	 */
	private $groupes;
	
	/**
	 * @var int
	 */
	private $groupesPlaces;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set intitule
     *
     * @param string $intitule
     *
     * @return EC
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * Get intitule
     *
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return EC
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set effectif
     *
     * @param integer $effectif
     *
     * @return EC
     */
    public function setEffectif($effectif)
    {
        $this->effectif = $effectif;

        return $this;
    }

    /**
     * Get effectif
     *
     * @return integer
     */
    public function getEffectif()
    {
        return $this->effectif;
    }

    /**
     * Set parcours
     *
     * @param string $parcours
     *
     * @return EC
     */
    public function setParcours($parcours)
    {
        $this->parcours = $parcours;

        return $this;
    }

    /**
     * Get parcours
     *
     * @return string
     */
    public function getParcours()
    {
        return $this->parcours;
    }

    /**
     * Set heures
     *
     * @param integer $heures
     *
     * @return EC
     */
    public function setHeures($heures)
    {
        $this->heures = $heures;

        return $this;
    }

    /**
     * Get heures
     *
     * @return integer
     */
    public function getHeures()
    {
        return $this->heures;
    }

    /**
     * Set typeCours
     *
     * @param string $typeCours
     *
     * @return EC
     */
    public function setTypeCours($typeCours)
    {
        $this->typeCours = $typeCours;

        return $this;
    }

    /**
     * Get typeCours
     *
     * @return string
     */
    public function getTypeCours()
    {
        return $this->typeCours;
    }

    /**
     * Add enseignant
     *
     * @param \Staps\UserBundle\Entity\Enseignant $enseignant
     *
     * @return EC
     */
    public function addEnseignant(\Staps\UserBundle\Entity\Enseignant $enseignant)
    {
        $this->enseignants[] = $enseignant;

        return $this;
    }

    /**
     * Remove enseignant
     *
     * @param \Staps\UserBundle\Entity\Enseignant $enseignant
     */
    public function removeEnseignant(\Staps\UserBundle\Entity\Enseignant $enseignant)
    {
        $this->enseignants->removeElement($enseignant);
    }

    /**
     * Get enseignants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnseignants()
    {
        return $this->enseignants;
    }

    /**
     * Set ue
     *
     * @param \Staps\CoursBundle\Entity\UE $ue
     *
     * @return EC
     */
    public function setUE(\Staps\CoursBundle\Entity\UE $ue)
    {
        $this->ue = $ue;

        return $this;
    }

    /**
     * Get ue
     *
     * @return \Staps\CoursBundle\Entity\UE
     */
    public function getUE()
    {
        return $this->ue;
    }
    
    /**
     * Add apsa
     *
     * @param \Staps\CoursBundle\Entity\APSA $apsa
     *
     * @return EC
     */
    public function addAPSA(\Staps\CoursBundle\Entity\APSA $apsa)
    {
        $this->apsas[] = $apsa;
        $apsa->addEC($this);

        return $this;
    }

    /**
     * Remove apsa
     *
     * @param \Staps\CoursBundle\Entity\APSA $apsa
     */
    public function removeAPSA(\Staps\CoursBundle\Entity\APSA $apsa)
    {
        $this->apsas->removeElement($apsa);
        $apsa->removeEC($this);
    }

    /**
     * Get apsas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAPSAs()
    {
        return $this->apsas;
    }
    
    public function setCartePlaces($cartePlaces) {
		$this->cartePlaces = $cartePlaces;

        return $this;
	}
	
	public function setCartePlacesCreneau($jour, $creneau, $idLieux, $idProfs) {
		$this->cartePlaces[$jour][$creneau]['lieux'] = $idLieux;
		$this->cartePlaces[$jour][$creneau]['profs'] = $idProfs;

        return $this;
	}
	
	public function getCartePlaces() {
		return $this->cartePlaces;
	}
	
	public function getNbPlaces() {
		$places = 0;		

		foreach ($this->cartePlaces as $jour) {
			foreach ($jour as $creneau) {
				$places += min(count($creneau['lieux']), count($creneau['profs']));
			}
		}

		return $places;
	}
	
	public function getNbPlacesCreneau($jour, $creneau) {
		return min(count($this->cartePlaces[$jour][$creneau]['lieux']), count($this->cartePlaces[$jour][$creneau]['profs']));
	}
	
	public function getNbGroupes($parametres) {
		$capaciteCours = $parametres->getCapacite($this->typeCours);
		
		$licence = ceil(substr($this->numero, 0, 1)/2);
		$effectifPrevu = $parametres->getEffectifPrevu($licence);

		$this->groupes = round(min($this->effectif, $effectifPrevu)/$capaciteCours);
		if ($this->groupes == 0) {
			$this->groupes = 1;
		}
		
		$this->effectifGroupe = min($this->effectif, $effectifPrevu)/$this->groupes;

		return $this->groupes;
	}
	
	public function addHeuresPlacees($ajout) {
		$this->heuresPlacees += $ajout;
	}
	
	public function estFixe() {		
		if (count($this->apsas) > 0) {
			foreach ($this->apsas as $a) {
				if (!$a->estFixe($this->heures)) {
					return false;
				}			
			}
			return true;
		}
		else {
			return $this->heuresPlacees >= $this->heures * $this->groupes;
		}
	}
	
	public function getGroupesPlaces() {
		return $this->groupesPlaces;
	}
	
	public function setGroupesPlaces($groupesPlaces) {
		$this->groupesPlaces = $groupesPlaces;
		
		return $this;
	}
	
	public function getEffectifGroupe() {
		return $this->effectifGroupe;
	}
	
	public function getEffectifPrevuLicence($parametres) {
		$licence = ceil(substr($this->numero, 0, 1)/2);
		return $parametres->getEffectifPrevu($licence);
	}
	
	public function estPlacable($parite, $coursPrecedent) {
		if ($coursPrecedent != null) {
			return $coursPrecedent->estFixe();
		}
		
		return $this->paire == null || $this->paire == $parite;
	}

    /**
     * Set code
     *
     * @param string $code
     *
     * @return EC
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set paire
     *
     * @param boolean $paire
     *
     * @return EC
     */
    public function setPaire($paire)
    {
        $this->paire = $paire;

        return $this;
    }

    /**
     * Get paire
     *
     * @return boolean
     */
    public function getPaire()
    {
        return $this->paire;
    }

    /**
     * Set coursPrecedent
     *
     * @param string $coursPrecedent
     *
     * @return EC
     */
    public function setCoursPrecedent($coursPrecedent)
    {
        $this->coursPrecedent = $coursPrecedent;

        return $this;
    }

    /**
     * Get coursPrecedent
     *
     * @return string
     */
    public function getCoursPrecedent()
    {
        return $this->coursPrecedent;
    }
}
