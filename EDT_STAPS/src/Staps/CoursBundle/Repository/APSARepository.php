<?php

namespace Staps\CoursBundle\Repository;

/**
 * APSARepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class APSARepository extends \Doctrine\ORM\EntityRepository {
	public function findByPartCode($code_apsa) {
		return $this->createQueryBuilder('apsa')
			->where('apsa.code LIKE :code_apsa')
			->setParameter('code_apsa', '%'.$code_apsa.'%')
			->getQuery()->getResult()
		;
	}
	
	public function deleteDouble() {
		$rawSql = "DELETE t1 
			FROM apsa AS t1, apsa AS t2
			WHERE t1.id > t2.id
			AND t1.code = t2.code
			AND t1.intitule = t2.intitule";
		
		$q = $this->getEntityManager()->getConnection()->prepare($rawSql);
		$q->execute();	
	}
	
	public function findByECs($ecs) {	
		$ec_ids = array();
		foreach ($ecs as $e) {
			$ec_ids[] = $e->getId();
		}
			
		return $this->createQueryBuilder('apsa')
			->innerJoin('apsa.ecs', 'ecs', 'WITH', 'ecs.id IN (:ec_ids)')
			->setParameter('ec_ids', $ec_ids)
			->getQuery()->getResult()
		;
	}
	
	public function findIdIntituleByECId($ec_ids) {				
		return $this->createQueryBuilder('apsa')
			->select('apsa.id AS apsa_id, apsa.intitule AS apsa_int')
			->innerJoin('apsa.ecs', 'ecs', 'WITH', 'ecs.id IN (:ec_ids)')
			->setParameter('ec_ids', $ec_ids)
			->getQuery()->getResult()
		;
	}
	
	public function findIdByEnseignant($prof) {
		if ($prof != null) {
			$resultats =  $this->createQueryBuilder('u')
				->select('u.id')
				->innerJoin('u.enseignants', 'profs', 'WITH', 'profs.id = :prof_id')
				->setParameter('prof_id', $prof->getId())
				->getQuery()->getResult()
			;
			
			// Permet d'obtenir un tableau sans clé
			$id = array();
			foreach ($resultats as $res) {
				$id[] = $res['id'];
			}
				
			return $id;
		}
		
		return array();
	}
}
