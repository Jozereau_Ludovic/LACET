<h1>Projet de stage UFR STAPS</h1>

<p><h2>Description du travail</h2>
L'objectif du stage était la création d'un logiciel capable de faire des emplois du temps particuliers pour les élèves de l'UFR STAPS.

Le projet consiste en un site web sur lesquels un administrateur, les enseignants et les étudiants ont la possibilité de renseigner certaines informations, 
utilisées par la suite pour la construction d'emplois du temps à la demande de l'administrateur.</p>

<p><h2>Technologies et langages utilisés</h2>
<ul><li>Symfony
    <li>MySQL
    <li>PHP
    <li>HTML
    <li>CSS
    <li>JavaScript
</ul></p>

<p><h2>Méthode de travail</h2>
Méthodologie Agile.
</p>

<p><h2>Rôles dans le projet</h2>
<ul><li>Programmeur
    <li>Concepteur
    <li>Web Designer
</ul></p>

<p><h2>Participants</h2>
<ul><li>Ludovic Jozereau
</ul></p>

<p><h3>Il n'y a pas d'exécutable pour ce projet.</h3></p>